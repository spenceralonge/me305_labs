'''!@file                    PulseChanger.py
    @brief                   A set of functions to pulse an LED with three different patterns
    @details                 Contains functions to switch the LED on a Nucleo to blinking on a one second square wave,
                             a one second ramp wave, and a 10 second sine wave. It also includes a sample program that allows the 
                             user to cycle between them in order by pressing the blue button on the nucleo. 
                             The demonstartion video can be found at https://youtu.be/_LrzPOHVKOM
    @author                  Scott Anderson, Spencer Alonge
    @copyright               License Info
    @date                    January 15, 2022
    
    @section sec_STDs   State Transition Diagrams
                        The finite state machines for Encoder and User tasks.
                        
                        For the task_encoder.py function, the finite state machine
                        looks like:
                            
                        @image html Task_Encoder.PNG
                        
                        For the Task_User.py function, the finite state machine is:  
                            
                        @image html Task_User.PNG
'''

import pyb
import math
import time

def callbackStateChange(IRQ_src):
    '''!
    @brief          Detects a button Press
    @details        This is a callback function to set the a global variable, 
                    buttonPress to true when the blue button on the face of the
                    Nucleo is pressed 
    @return         This fuction does not return anything
    '''
    #global boolean to flag when the button is pressed
    global buttonPress
    buttonPress = True
   

def squareWave(t):
    '''!
    @brief          Creates a square wave
    @details        Given a time value in ms, it will output either the maximum
                    value of 100 or the minimum value of 0, switching every 
                    half second
    @param t        The current time of the wave in milliseconds
    @return         Returns a value intended to be used as the brightness of
                    an LED, between 0 and 100
    '''
    #if the time is less than .5s, return max brightness, otherwise 0 brightness
    #the modulus effectively loops the time between 0 and 1 sec
    if (t%1000<500):
        return 100
    else:
        return 0
    
    
def sinWave(t):
    '''!
     @brief          Creates a sine wave
     @details        Given a time value in ms, it will output a sine wave
                     that starts at 50 at zero ms, goes up to 100 at 2.5s, and
                     completes the wave at 50 by 10s
     @param t        The current time of the wave in milliseconds
     @return         Returns a value intended to be used as the brightness of
                     an LED, between 0 and 100
     '''
    #time input is turned into a sine wave starting at 50, looping every 
    #10 seconds 
    return 50*math.sin(2*3.1415*((t%10000)/10000)) + 50
    
def triWave(t):
    '''!
     @brief          Creates a ramping trianle wave
     @details        Given a time value in ms, it will output in a line from 0 
                     at 0 ms, to 100 at one ms.
     @param t        The current time of the wave in milliseconds
     @return         Returns a value intended to be used as the brightness of
                     an LED, between 0 and 100
     '''
    #turns the current time into a simple ramp from 0 brightness to 100 brightness
    #it loops every second
    return (t%1000)/10

#Code to demonstrate these wave functions by cycling between them on a button
#press. Consists of a state machine with 4 states: s0: waiting, s1:square wave,
#s2: sine wave, and s3: triangle wave. 
if __name__ == '__main__':
    
    #define the LED pin
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    
    #define the button pin
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    
    #setup timer object to use for PWM
    tim2 = pyb.Timer(2, freq=20000)
    
    #setup PWM using tim2 timer and pinA5
    t2ch1 = tim2.channel(1,pyb.Timer.PWM, pin=pinA5)
    
    #default starting state
    state = 0
    
    #button unpressed at start of program
    buttonPress = False
    
    #initial brightness 0(off)
    brt = 0
    
    #initial time 0 
    then = 0
    
    #Setup the button callback for the falling stroke of the button on pinC13
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=callbackStateChange)
    
    #welcome message
    print('Welcome, please press the blue button to change the LED blinking pattern from square to sine to triangular waves and press cntrl+C to terminate the program')
    
    #loop for the state machine
    while True:
        try:
            #set the current time to now
            now = time.ticks_ms() 
            
            #each state sets 'then' to the current time when tranistioning, in
            #in order to start each wave from the 0 sec
            #each state tranistions to the next on button press and prints a
            #message to inform the user while resetting the global boolean
            #each state feeds the current time through a different wave 
            #function and outputs brightness
            
            #waiting state
            if state == 0:
                if buttonPress:
                    print('switching to square wave')
                    state = 1
                    then = time.ticks_ms()
                    buttonPress = False
            #square wave state
            elif state == 1:
                brt = squareWave(now - then)
                if buttonPress:
                    state = 2
                    print('switching to sine wave')
                    then = time.ticks_ms()
                    buttonPress = False
            #sine wave state
            elif state == 2:
                brt = sinWave(now - then)
                if buttonPress:
                    state = 3
                    print('switching to triangular wave')
                    then = time.ticks_ms()
                    buttonPress = False
            #ramp wave state
            elif state == 3:
                brt = triWave(now - then)
                if buttonPress:
                    state = 1
                    print('switching to square wave')
                    then = time.ticks_ms()
                    buttonPress = False
            
            #sets the LED brightness to current brt value
            t2ch1.pulse_width_percent(brt)
            
            #sleep .1 seconds between each loop of the state machine
            time.sleep(0.01)
                
        #break the loop, turn off the LED, and display terminate message on ctrl+C        
        except KeyboardInterrupt:
            print('terminate')
            t2ch1.pulse_width_percent(0)
            break
