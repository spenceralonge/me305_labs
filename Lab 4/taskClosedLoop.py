'''!@file           taskClosedLoop.py
    @brief          A file to set closed loop duty cycles for the motor.
    @details        This file sets up a task to perform motor control during
                    closed loop control mode. It constantly calculates what the
                    duty cycle of the motor should be to reach a designated value
                    taken from user input. It does this with user input gains and
                    an encoder read velocity. It has only two states. State 1
                    initializes the motor controller and then goes to State 1.
                    State 1 constantly updates the user set values in case of
                    changes and then calculates the necessary duty cycle and
                    outputs it to the user via a shared variable. For a state
                    transition diagram, reference the following:
                        
                    @image html Lab4CLCStates.JPG
                    
                    As always, feel free to reference the source code at
                    https://bitbucket.org/seanwahl2023/me_305_labs/src/master/Lab%204/
                    however especially in this case since function variables
                    are not captured by Doxygen.
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           February 23, 2022
'''

import micropython, closedloop
from time import ticks_us, ticks_add, ticks_diff

def taskClosedLoopFunction(taskName, period, L, Kp, wMeas, wRef, Ki):
    '''!@brief              Routinely calculates the necessary duty cycle for a motor.
        @details            In closed loop control, the controller takes in the
                            value of the current motor velocity and the value
                            the velocity should be at in order to calculate the
                            required gain to bring the motor to that value. This
                            is done with user specified gains that control how
                            fast the motor gets to this desired speed within
                            reason. If the gains are too high, the motor speed
                            oscillates unpredictably and has trouble coming to
                            steady state. The two states of this task are only
                            to initialize the controller and to constantly update
                            the duty cycle.
        @param taskName     The arbitrary, chosen name of the task.
        @param period       The time interval at which this task should operate.
        @param L            A shared variable for inputting the duty cycle the
                            motor should run at.
        @param Kp           A shared variable holding the user input Kp gain.
        @param wMeas        A shared variable holding the encoder read motor velocity.
        @param wRef         A shared variable holding the user input desired motor velocity.
        @param Ki           A shared variable holding the user input Ki gain.
    '''
    
    ## @brief Represents the initialization state of the task.
    #
    S0_INIT = micropython.const(0)
    
    ## @brief Represents the updating state of the task.
    #
    S1_UPD = micropython.const(1)
    
    ## @brief       Denotes the state that the task is currently running in.
    #  @details     taskClosedLoop has two possible states. State 0 initializes
    #               the motor controller and always sends the task to state 1. 
    #               In state 1, the motor controller constantly has its gains
    #               updated to match user input and then calculates the duty
    #               duty cycle the motor should be running at. This should be
    #               done frequently to achieve accurate closed loop control.
    #
    state = S0_INIT
    
    ## @brief The initial time at which the task has first started running.
    #
    start_time = ticks_us()
    
    ## @brief       The next time the task should run.
    #  @details     By using the set period input, next_time is a calculated
    #               value of when the task should run again in order to be
    #               operating at said period.
    #
    next_time = ticks_add(start_time, period)

    
    while True:
        ## @brief The current time when the function tries to run again.
        #
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:    
            
            next_time = ticks_add(next_time, period)
            
            # Sets up motor controller object.
            if state == S0_INIT:
                ## @brief The motor controller object that is set up using the closedloop class.
                #
                controller = closedloop.ClosedLoop(180)
                _wRefpast = wRef.read()
                state = S1_UPD
                yield None
            
            # Constantly updates the gains and required duty cycle
            elif state == S1_UPD:
                controller.set_gain(Kp.read(), Ki.read())
                L.write(controller.run(wRef.read(), wMeas.read()))
                
                # Resets the integral control offset if the desired speed is
                # changed or if the motor is stopped.
                if wMeas.read() == 0: controller._errsum = 0
                if wRef.read() != _wRefpast: controller._errsum = 0
                _wRefpast = wRef.read()
            
            # Just in case
            else:
                print('something went wrong')
                break
        else:
            yield None
