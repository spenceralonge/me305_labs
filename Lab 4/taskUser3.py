'''!@file           taskUser3.py
    @brief          A file to take user input and communicate with the other tasks.
    @details        taskUser sets up a generator function to run on a specified
                    period. This function takes in user specified values and
                    performs the designated operation for their input. It also
                    can send or retrieve values from taskEncoder, taskMotor, and
                    taskClosedLoop in order to create a well-oiled machine for
                    motor control. taskUser is like the control task for the system. For a
                    breakdown of the states, please check out the function details.
                    For a more comprehensive understanding of the state transitions,
                    reference the following diagram:
                        
                    @image html Lab4UserStates.JPG
                    
                    As always, feel free to reference the source code at
                    https://bitbucket.org/seanwahl2023/me_305_labs/src/master/Lab%204/
                    however especially in this case since function variables
                    are not captured by Doxygen.
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           February 23, 2022
'''

from time import ticks_us, ticks_add, ticks_diff
import pyb, micropython, array, gc

def taskUserFunction(taskName, period, zFlag, position, delta, timeVal, velocity, DC1, DC2, cFlag, err, L, Kp, wMeas, wRef, wFlag, Ki):
    '''!@brief              Routinely checks for user input and acts accordingly.
        @details            This generator function, given the right set of
                            inputs, is able to constantly check for user input
                            and will change state based on what the user specifies.
                            It reads shared variables that taskEncoder, taskMotor,
                            and taskClosedLoop write to in order to relay data
                            to the user. It can also write to these variables
                            to give user info to the tasks themselves. There are 
                            13 various states that taskUser can operate in. 
                            Please just reference the Lab 4 page or the taskUser3.py 
                            file for the state transition diagram documenting all 
                            the states.
        @param taskName     The arbitrary, chosen name of the task.
        @param period       The time interval at which this task should operate.
        @param zFlag        A shared variable that means the encoder should be zeroed.
        @param position     A shared variable that holds the current position value.
        @param delta        A shared variable that holds the last delta value.
        @param timeVal      A shared variable that holds local time values from
                            taskEncoder.
        @param velocity     A shared variable that holds the angular velocity
                            of the encoder.
        @param DC1          A shared variable that holds the duty cycle for motor 1.
        @param DC2          A shared variable that holds the duty cycle for motor 2.
        @param cFlag        A shared variable that means the encoders should be
                            renabled to reset a fault.
        @param err          A shared variable indicating a motor fault has occured.
        @param L            A shared variable holding the required duty cycle
                            for closed loop motor control.
        @param Kp           A shared variable holding the standard gain for closed
                            loop motor control.
        @param wMeas        A shared variable holding the measured velocity
                            of the motor from the encoder, like the velocity variable.
        @param wRef         A shared variable holding the user input desired
                            velocity for the motor for closed loop control.
        @param wFlag        A shared variable used for indicating when closed
                            loop control should be active.
        @param Ki           A shared variable holding the user input closed loop
                            integral control gain.
    '''
    
    ## @brief Represents the initialization state of the task.
    #
    S0_INIT = micropython.const(0)
    
    ## @brief Represents the command center state of the task.
    #
    S1_CMD = micropython.const(1)
    
    ## @brief Represents the zeroing state of the task.
    #
    S2_ZERO = micropython.const(2)
    
    ## @brief Represents the duty cycle specifying state.
    #
    S3_DUTY = micropython.const(3)
    
    ## @brief Represents the fault clearing state of the task.
    #
    S4_CLEAR = micropython.const(4)
    
    ## @brief Represents the data collecting state of the task.
    #
    S5_DATA = micropython.const(5)
    
    ## @brief Represents the data printing state of the task.
    #
    S6_PRINT = micropython.const(6)
    
    ## @brief Represents the duty cycle recording state.
    #
    S7_DCREC = micropython.const(7)
    
    ## @brief Represents the desired velocity setting state for closed loop control.
    #
    S8_CLC = micropython.const(8)
    
    ## @brief Represents the Kp setting state.
    #
    S9_KP = micropython.const(9)
    
    ## @brief Represents the Ki setting state.
    #
    S10_KI = micropython.const(10)
    
    ## @brief Represents the step function state for closed loop control.
    #
    S12_STEP = micropython.const(12)
    
    ## @brief Represents the state in which numerical values are read.
    #
    S11_READ = micropython.const(11)

    
    ## @brief       Denotes the state that the task is currently running in.
    #  @details     taskUser has 13 possible states. Please see the taskUser3.py
    #               file for details on the states.
    #
    state = S0_INIT
    
    ## @brief Contains serial inputs from the keyboard.
    #
    serport = pyb.USB_VCP()
    
    ## @brief The time at which the task is first run.
    #
    start_time = ticks_us()
    
    ## @brief The time at which the task should start its next cycle.
    #
    next_time = ticks_add(start_time, period)
    
    ## @brief An array of time values taken during data collection.
    #
    timeArray = array.array('f', 1501*[0])
    gc.collect()
    
    ## @brief An array that can be used for any values taken during data collection.
    #
    data1Array = array.array('f', timeArray)
    
    ## @brief A secondary array that can be used for any values taken during data collection.
    #
    data2Array = array.array('f', timeArray)
    
    ## @brief A time used to keep track of data colletion.
    #
    t = ticks_diff(ticks_us(),1000000)
    
    while True:
        ## @brief The time at the beginning of the current task run.
        #
        current_time = ticks_us()
        
        # Check if alarm clock went off
        if ticks_diff(current_time, next_time) >= 0:
            next_time = ticks_add(next_time, period)
            
            # Initialize
            if state == S0_INIT:
                _printHelp()
                ## @brief The string of numbers input during the number reading state.
                #
                num = ''
                state = S1_CMD
                ## @brief Indicates what kind of data should be 
                #         printed in the print state.
                printVar = 0
                
            # Command Center
            elif state == S1_CMD:
                if serport.any():
                    ## @brief Character data from user's keyboard inputs.
                    #
                    charIn = serport.read(1).decode()
                    
                    #Set state according to user input
                    if charIn in {'z','Z'}:
                        print('Zeroing the Encoder')
                        state = S2_ZERO
                        yield zFlag.write(True)

                    elif charIn in {'p','P'}:
                        print(f'The encoder is at {position.read():.1f} [rad]')
                        yield None
                        
                    elif charIn in {'d','D'}:
                        print(f'The last delta was {delta.read():.3f} [rad]')
                        yield None
                        
                    elif charIn in {'v','V'}:
                        print(f'The last velocity was {velocity.read():.1f} [rad/s]')
                        yield None
                        
                    elif charIn in {'g','G'}:
                        ## @brief Index used in data collection to see how many
                        #        data points have been taken.
                        #
                        idx = 0
                        state = S5_DATA
                        yield None
                        
                    elif charIn in {'m','M'}:
                        if charIn == 'm':
                            ## @brief Variable to indicate which motor's DC should
                            #         be set.
                            #
                            motor = 1
                            serport.write('Input duty cycle for motor 1 [%]: ')
                        elif charIn == 'M':
                            motor = 2
                            serport.write('Input duty cycle for motor 2 [%]: ') 
                        ## @brief Indicates which state should be transitioned
                        #         to after the number reading state, State 11.
                        #
                        stateQueue = S3_DUTY 
                        state = S11_READ
                        yield None
                        
                    elif charIn in {'s','S'}:
                        break
                        yield None
                        
                    elif charIn in {'c','C'}:
                        cFlag.write(True)
                        state = S4_CLEAR
                        yield None
                       
                    elif charIn in {'t','T'}:
                        ## @brief Variable used for the DC collection state.
                        #         Sets what the duty cycle was input as.
                        #
                        setvar = True
                        ## @brief Variable used for the DC collection state.
                        #         Indicates what should be done next.
                        #
                        startvar = False
                        stateQueue = S7_DCREC
                        state = S11_READ
                        idx = 0
                        printVar = 1
                        print('Starting data collection.')
                        serport.write('Enter a duty cycle [%]: ')
                        yield None
                        
                    elif charIn in {'y','Y'}:
                        serport.write('Choose a reference velocity [rad/s]: ')
                        stateQueue = S8_CLC
                        state = S11_READ
                        num = ''
                        yield None
                        
                    elif charIn in {'k','K'}:
                        serport.write('Choose closed loop gain Kp [%*s/rad]: ')
                        stateQueue = S9_KP
                        state = S11_READ
                        yield None
                        
                    elif charIn in {'r','R'}:
                        
                        idx = 0
                        ## @brief Index used to progress along the steps in the
                        #         step function state.
                        #
                        stepTask = 1
                        DC1.write(0)
                        serport.write('Enter a desired velocity [rad/s]: ');
                        wFlag.write(False)
                        stateQueue = S12_STEP
                        state = S11_READ
                        yield None
                    
                    elif charIn in {'w','W'}:
                        wFlag.write(not wFlag.read())
                        if wFlag.read():
                            print('Closed loop control enabled.')
                        else:
                            print('Closed loop control disabled.')
                        yield None
                    
                    else:
                        print(f'{charIn} is not a valid input. Please try again.')
                        yield None
                        
                elif err.read():
                    print('Motor error has occured, please press c.')
                    err.write(False)
                    yield None
                    
                else:
                    yield None
                    
            # Zeroing state
            elif state == S2_ZERO:
                if not zFlag.read():
                    print('Encoder value set to zero.')
                    state = S1_CMD
                    yield None
                else:
                    yield None
             
            # Read numerical values        
            elif state == S11_READ:
                if serport.any():
                    charIn = serport.read(1).decode()
                    if charIn in {'s','S'}:
                        state = S1_CMD
                        print('Cancelling.')
                    elif charIn.isdigit():
                        num += charIn
                        serport.write(charIn)
                    elif charIn == '-':
                        if len(num) == 0:
                            num += charIn
                            serport.write(charIn)
                        else:
                            yield None
                    elif charIn in {'\x2E', '\x46'}:
                        if charIn not in num:
                            num += charIn
                            serport.write(charIn)
                            yield None
                        else:
                            yield None
                    elif charIn in {'\b','\x08', '\x7F'}:
                        if len(num) == 0:
                            yield None
                        else:
                            num = num[:-1]
                            serport.write('\x7F')
                    elif charIn in {'\r','\n'}:
                        if len(num) == 0:
                            pass
                        else:
                            serport.write('\n \r')
                            ## @brief Holds the user input number as a float.
                            #
                            buf = float(num)
                            num = ''
                            state = stateQueue
                            yield None                            
                else:
                    yield None        
            
            # State to change duty cycle of motor 1 or 2.
            elif state == S3_DUTY:
                if motor == 1:
                    DC1.write(buf)
                    print(f'Motor 1 duty cycle set to {DC1.read()}')
                else:
                    DC2.write(buf)
                    print(f'Motor 2 duty cycle set to {DC2.read()}')
                state = S1_CMD
                yield None

            # State to clear motor faults.
            elif state == S4_CLEAR:
                if not cFlag.read():
                    print('Fault cleared, waiting for command.')
                    state = S1_CMD
                    yield None
                else:
                    yield None
                    
            # Data collection state
            elif state == S5_DATA:                
                # start a data timer
                if idx == 0:
                    print('Press s/S to stop data collection early.')
                    data1Array[idx] = position.read()
                    data2Array[idx] = velocity.read()
                    timeArray[idx] = 0
                    t = timeVal.read()
                    idx += 1
                    yield None                    
                # check for interrupt
                elif serport.any():
                    charIn = serport.read(1).decode()
                    if charIn in {'s','S'}:
                        data1Array[idx] = position.read()
                        data2Array[idx] = velocity.read()
                        timeArray[idx] = ticks_diff(timeVal.read(),t)/1_000_000
                        state = S6_PRINT
                        ## @brief Keeps track of how many data points have been printed.
                        #
                        numPrinted = 0
                        yield None    
                    else:
                        data1Array[idx] = position.read()
                        data2Array[idx] = velocity.read()
                        timeArray[idx] = ticks_diff(timeVal.read(),t)/1_000_000
                        idx += 1
                        yield None        
                # check for end case    
                elif ticks_diff(timeVal.read(),t) > 7_500_000:
                    state = S6_PRINT
                    numPrinted = 0
                    yield None               
                else:
                    data1Array[idx] = position.read()
                    data2Array[idx] = velocity.read()
                    timeArray[idx] = ticks_diff(timeVal.read(),t)/1_000_000
                    idx += 1
                    yield None
            
            # Data printing state
            elif state == S6_PRINT:
                if printVar == 1:
                    if numPrinted == idx - 1:
                        print(f'{data1Array[numPrinted]:.1f}, {data2Array[numPrinted]:.2f}')
                        state = S1_CMD
                        printVar = 0
                        print('End of Data Collection.')
                        _printHelp() 
                    else:
                        print(f'{data1Array[numPrinted]:.1f}, {data2Array[numPrinted]:.2f}')
                        numPrinted +=1
                elif printVar == 0:
                    if numPrinted == idx - 1:
                        print(f'{timeArray[numPrinted]:.3f}, {data1Array[numPrinted]:.1f}, {data2Array[numPrinted]:.2f}')
                        state = S1_CMD
                        print('End of Data Collection.')
                        _printHelp()
                        yield None                       
                    else:
                        print(f'{timeArray[numPrinted]:.3f}, {data1Array[numPrinted]:.1f}, {data2Array[numPrinted]:.2f}')
                        numPrinted += 1
                        yield None
                        
            # DC value specifying and angular velocity collecting state.    
            elif state == S7_DCREC:
                if setvar == True:
                    t = ticks_us()
                    DC1.write(buf)
                    ## @brief List used to hold velocity values before they are averaged.
                    #
                    tempArray = []
                    setvar = False
                    yield None
                elif err.read():
                    print('Motor error has occured, ending data collection prematurely, please press c.')
                    err.write(False)
                    state = S1_CMD
                    yield None
                elif 500_000 <= ticks_diff(ticks_us(),t) < 800_000:
                    tempArray.append(velocity.read())
                    if ticks_diff(ticks_us(),t) > 750_000:
                        t = ticks_add(t,t)
                        data2Array[idx] = sum(tempArray)/len(tempArray)
                        data1Array[idx] = DC1.read()
                        serport.write('Continue DC Collection? (Y/N): ')
                        startvar = 1
                        yield None
                    else:
                        yield None
                elif startvar == True:
                    if serport.any():
                        charIn = serport.read(1).decode()
                        if charIn in {'y','Y'}:
                            serport.write(f'{charIn} \n \r')
                            serport.write('Enter a duty cycle [%]: ')
                            stateQueue = S7_DCREC
                            state = S11_READ
                            setvar = True
                            startvar = False
                            idx += 1
                            yield None
                        elif charIn in {'n','N'}:
                            serport.write(f'{charIn} \n \r')
                            idx += 1
                            state = S6_PRINT
                            numPrinted = 0
                            yield None
                        else:
                            yield None
        
            # Closed loop reference velocity setting.
            elif state == S8_CLC:
                wRef.write(buf)
                state = S1_CMD
                yield None
            
            # Closed loop Kp gain setting.
            elif state == S9_KP:
                Kp.write(buf)
                print(f'Kp value set to {Kp.read()} [%*s/rad]')
                serport.write('Choose gain Ki [Hz]: ')
                stateQueue = S10_KI
                state = S11_READ
                yield None
            
            # Closed loop Ki gain setting.
            elif state == S10_KI:
                Ki.write(buf)
                print(f'Ki value set to {Ki.read()} [Hz]')
                state = S1_CMD
                yield None
            
            # Closed loop step function operation.
            elif state == S12_STEP:
                if stepTask == 1:
                    wRef.write(buf)
                    serport.write('Enter a Kp gain [%*s/rad]: ')
                    state = S11_READ
                    stateQueue = S12_STEP
                    stepTask += 1
                    yield None
                elif stepTask == 2:
                    Kp.write(buf)
                    serport.write('Enter a Ki gain [Hz]: ')
                    state = S11_READ
                    stateQueue = S12_STEP
                    stepTask += 1
                    yield None
                elif stepTask == 3:
                    Ki.write(buf)
                    t = ticks_us()
                    print('Performing step function.')
                    stepTask += 1
                    yield None
                elif stepTask == 4:
                    if ticks_diff(ticks_us(),t) < 1_000_000:
                        timeArray[idx] = ticks_diff(ticks_us(),t)/1_000_000
                        data1Array[idx] = DC1.read()
                        data2Array[idx] = velocity.read()
                        idx += 1
                        yield None
                    elif ticks_diff(ticks_us(),t)<3_000_000:
                        wFlag.write(True)
                        if err.read():
                            print('Motor error has occured, ending data collection prematurely, please press c.')
                            err.write(False)
                            state = S1_CMD
                            wFlag.write(False)
                            yield None
                        else:
                            timeArray[idx] = ticks_diff(ticks_us(),t)/1_000_000
                            data1Array[idx] = L.read()
                            data2Array[idx] = velocity.read()
                            idx += 1  
                            yield None
                    else:
                        wFlag.write(False)
                        state = S6_PRINT
                        numPrinted = 0
                        yield None
                    
                else:
                    yield None
        else:
            yield None
        
def _printHelp():
    print('+------------------------------------------------------------------+')
    print('|    User Interface:  Please select an input                       |')
    print('+------------------------------------------------------------------+')
    print('| Input |                         Function                         |')
    print('+------------------------------------------------------------------+')
    print('| z / Z |  Zero the position of encoder 1.                         |')
    print('| p / P |  Retrieve the position of encoder 1.                     |')
    print('| d / D |  Retrieve the delta for encoder 1.                       |')
    print('| v / V |  Retrieve the velocity for encoder 1.                    |')
    print('|   m   |  Enter a duty cycle (DC) for motor 1.                    |')
    print('|   M   |  Enter a DC for motor 2.                                 |')
    print('| c / C |  Clear DRV8847 fault condition.                          |')
    print('| g / G |  Collect encoder 1 data for fifteen seconds.             |')
    print('| t / T |  Motor 1 DC testing interface.                           |')
    print('| y / Y |  Choose a set point for closed-loop control for motor 1. |')
    print('| k / K |  Enter a gain for closed-loop control for motor 1.       |')
    print('| w / W |  Enable or disable closed-loop control.                  |')
    print('| r / R |  Perform a step response on motor 1.                     |')
    print('| s / S |  Exit data collection modes or interface.                |')
    print('|Ctrl+C |  Exit program.                                           |')
    print('+------------------------------------------------------------------+')