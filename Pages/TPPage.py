'''! @page TPPage   Term Project - Ball-Balancing Platform
    @section sec_TPhome  Project Home
                        For our term project, we built off of our lab 5 code and
                        implemented new components in order to fully balance
                        our platform with a ball on it. Please watch the video below
                        for a preview of the ball balancing.
                        
                        If the inbedded video does not work, use the following link:
                        https://www.youtube.com/watch?v=BT8gIx_PZtM
                        
                        \htmlonly
                        
                        <center> <iframe width="750" height="500" src="https://www.youtube.com/watch?v=BT8gIx_PZtM" frameborder="0" allowfullscreen></iframe>
                          </iframe> </center>
                        \endhtmlonly    
                        
                        There's a lot going on in this project, so feel free
                        to browse around the sub pages as you see fit.
                        
                        @ref DemonstrationTP - Goes over the platform's user interface
                        and how to use the programs we've created in depth so that
                        anyone can use them if they have access to the technology.
                        
                        @ref GeneralTP - Covers all term project files and shows
                        their state machines, class diagrams, and task diagram.
                        
                        @ref DataTP - Contains our three data-sets avaiable to
                        be collected, and what their trends show.
                        
                        
    @author             Sean Wahl
    @author             Grant Gabrielson
    @author             Spencer Alonge
    @date               March 16, 2022
'''