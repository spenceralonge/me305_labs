'''! @page HW0x02       Ball-Balancing Platform Equation Derivation
    @section sec_gen    Overview
                        For Homework 0x02, we were tasked with deriviving
                        equations describing the motion of a ball balancing
                        on a platform controlled via motors and lever arms. In
                        the end, it is ideal to describe this motion solely in
                        terms of the platform's angular position, velocity, and
                        acceleration along with the balls position, velocity,
                        and acceleration relative to the platform.
                        
                        We can achieve this by making several simplifying
                        assumptions including that the axes of the platform
                        are uncoupled, and like the push rods from the motors
                        to the platform remain entirely vertical. With these,
                        we can independtly analyze one motor's effect on the
                        system and make a matrix solution in the form Mx = f
                        so that we can apply it to the other motor about
                        the other set of axes. Armed with this, we should later
                        be able to effectively simulate the ball's motion on
                        the platform.
                        
    @section sec_eqs    Hand-Worked Solution
                        In the remainder of this document, you will be presented
                        with the complete solution of the derivation of the 
                        ball's motion that I have carried out, with additional
                        guidance from Professor Charlie Refvem.
                        
                        \htmlonly
                        <center> <embed src="Alonge_Spencer_ME305_HW0x02.pdf" width="850px" height="1000px" href="Alonge_Spencer_ME305_HW0x02.pdf"></embed> </center>
                        \endhtmlonly
                        
    @author             Sean Wahl
    @author             Spencer Alonge
    @date               February 11, 2022
'''