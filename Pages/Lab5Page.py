'''! @page Lab5Page Lab 5 - I2C and Inertial Measurement Units
    @section sec_gen    Overview
                        In Lab 5, we repurposed our lab 3 code in order to do
                        closed loop control on a platform's position instead of
                        a motor's rotational speed. In order to do this we had
                        to learn a lot about I2C and Inertial Measurement Units
                        (IMUs) in order to obtain the platform's angles and 
                        angular velocities.
                        
                        A brief description of all files used are listed below:
                            
                        Lab5Main.py - Calls the function files and creates 
                                      shared variables used to communicate
                                      between the files. Runs the tasks by
                                      calling for the next input from each.
                                      
                        taskUser4.py - Creates a user interface and handles all
                                      user input. Also does all the printing
                                      of data retrieved from other tasks.
                                      
                        taskBNO.py - Uses the IMU class in order to calibrate
                                     the IMU and then constantly retrieve euler
                                     angles and angular velocities from the platform.
                                         
                        BNO055.py - Sets up an IMU using I2C communication and
                                     gives it several important methods that
                                     relate to calibrating and retrieving data
                                     from the chip. See BNO055.BNO055 for details.
                            
                        taskMotor3.py - Sets up a motor driver and motor objects.
                                       Is able to set motor duty cycles based
                                       on user input taken in taskUser3.
                                     
                        motor3.py - Defines a motor class with the ability to change
                                   motor duty cycles through pulse-width modulation.
                                   Needs pin objects corresponding to the desired
                                   motor and a timer object with correct timer
                                   channels. See motor3.Motor for details.
                                     
                        taskClosedLoop2.py - Sets up a motor controller object and
                                    calculates appropriate duty cycles (torques)
                                    to level the platform.
                                    
                        closedloop2.py - Defines a motor controller for closed loop
                                    motor control. Is able to do the calculations
                                    that taskClosedLoop returns. See closedloop2.ClosedLoop
                                    for details.
                                     
                        @ref shares.py - Sets up a sharing class in order to create
                                    variables accessible from multiple files.
                                    Also can create queues, however this was not
                                    utilized here. Provided courtesy of Charlie
                                    Refvem at Cal Poly.
                        
    @section sec_STDs   Finite State Machines
                        The finite state machines for the generator functions
                        are very important as they represent the logic behind
                        the code we've written.
                        
                        For the taskBNO.py function, the finite state machine
                        looks like:
                            
                        @image html Lab5BNOStates.JPG
                        
                        For the taskMotor3.py function, the finite state machine
                        is as follows:
                            
                        @image html Lab5MotorStates.JPG
                        
                        For the taskClosedLoop2.py function, the finite state machine
                        is:
                            
                        @image html Lab5CLCStates.JPG
                        
                        For the taskUser4.py function, the finite state machine is:  
                            
                        @image html Lab5UserStates.JPG
                        
     @section sec_ODs    Other Diagrams          
                        In addition to the finite state machines we also made a
                        task diagram to showcase the overall communication
                        between tasks. The Task Diagram for the main file is as 
                        pictured below:
                            
                        @image html Lab5TD.JPG
                        
                        We also made several class diagrams, one for each class:
                            
                        @image html Lab5MotorClass.JPG
                        @image html Lab5BNOClass.JPG
                        @image html Lab5CLCClass.JPG
                        
     @section sec_Vid   Closed Loop Control Tuning
                        Closed loop control is a very useful tool, and though
                        the gains used in our PID controller can be theoretically
                        estimated, the best way to tune the platform is to slowly
                        change the proportional gain until the behavior is good
                        enough, and then do the same with the derivative gain
                        and then lastly the integral gain.
                        
                        We do not have any fancy plots from this process this
                        time, but we do have a video of the platform balancing 
                        itself upon being disturbed. The gains used in this video
                        were Kp = 3.5 [%/deg], Kd = 0.25 [%*s/deg], Ki = 10 [Hz].
                        
                        If the inbedded video does not work, use the following link:
                        https://www.youtube.com/watch?v=sX5wBp-TV_c
                        
                        \htmlonly
                        
                        <center> <iframe width="750" height="500" src="https://www.youtube.com/watch?v=sX5wBp-TV_c" frameborder="0" allowfullscreen></iframe>
                          </iframe> </center>
                        \endhtmlonly
                        
    @author             Sean Wahl
    @author             Grant Gabrielson
    @author             Spencer Alonge
    @date               March 3, 2022
'''