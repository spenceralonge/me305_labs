'''!@file                mainpage.py
    @brief               Welcome to my portfolio!
    @details             In this portfolio I will be documenting all of my
                         coding work and projects for future reference/ display
                         for all.

    @mainpage
    
    @section sec_repo   Repository Reference
                        All code that will be referenced in this portfolio
                        is accessible through https://bitbucket.org/spenceralonge/me305_labs/src/master/. 
                        
                        However, you may find it more useful to read through
                        the labs before looking around there.
                        
    @section sec_Lab0  Lab 0 - Fibonacci
                        Lab 0 returns a Fibonacci number depending
                        on the user-specified index. See Alonge_Lab_0x00.py
                        for details.

    @section sec_Lab1  Lab 1 - Getting Started with Hardware
                        Lab 1 was manipulating an LED's light patterns through
                        button presses. See PulseChanger.py for more info.

    @section sec_Lab2  Lab 2 - Incremental Encoders
                        In Lab 2, we created and encoder class to set up an
                        encoder with various functions. We then made tasks to
                        communicate with each other with one grabbing data from
                        the encoder, and the other spitting out the data to the
                        user.
                        
                        Reference @ref Lab2Page for details.
                        
    @section sec_Lab3  Lab 3 - DC Motor Control
                        In Lab 3, we created a motor class to create motor objects
                        that can have their duty cycles set. We then created a
                        motor task to run with our previous tasks and classes
                        from Lab 2 which all work together to record data and
                        set motor duty cycles based on user input.
                        
                        Reference @ref Lab3Page for details.
                        
    @section sec_Lab4  Lab 4 - Closed Loop Motor Control
                        In Lab 4, we created a closed loop motor controller class
                        and a corresponding task in order to be able to set motor
                        velocities to a designated speed. In doing so we built off
                        of previous lab files from Lab 3 and in the process made
                        the code more efficient than it was previously.
                        
                        Reference @ref Lab4Page for details.
                        
    @section sec_Lab5  Lab 5 - I2C and Inertial Measurement Units
                        In Lab 5, we created an IMU class in order to retrieve
                        angles and angular velocity from our platform. With these,
                        we could translate our closed loop control from last time
                        to balance/level the platform. A little bit of research
                        on our BNO055 was needed for this, and so was some tuning
                        to get the gain values to be adequate.
                        
                        Reference @ref Lab5Page for details.         
                        
     @section sec_TP    Term Project - Ball-Balancing Platform
                         For our term project, we were tasked with balancing a
                         ball on top of a pivoting platform. To do this we had
                         to use everything we've learned so far and to implement
                         new components such as a touch panel in order to get this
                         working. This was definitely the most in depth project
                         we have done in this field so far.
                         
                         Reference @ref TPPage for details.
                        
     @section sec_HW   Homework and Other Files
                        @ref HW0x02 - This file goes over the equation of motion
                        derivation for the ball-balancing platform.
                        
                        @ref HW0x03 - This file simulates the ball balancing
                        platform's response based on HW 0x02's equations.
                        
    @author              Sean Wahl
    @author              Grant Gabrielson
    @author              Spencer Alonge

    @date                March 16, 2022
'''