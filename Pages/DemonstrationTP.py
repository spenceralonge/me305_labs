'''! @page DemonstrationTP  Term Project Ball-Balancer UI and Demonstration
    @section sec_demo   Demonstration
                        Please watch the video below for an idea of how to use
                        our code in order to effectively use the ball balancer.
                        If the inbedded video does not work, use the following link:
                        https://www.youtube.com/watch?v=zUB4KfRw6h0
                        
                        \htmlonly
                        
                        <center> <iframe width="750" height="500" src="https://www.youtube.com/watch?v=zUB4KfRw6h0" frameborder="0" allowfullscreen></iframe>
                          </iframe> </center>
                        \endhtmlonly    
                        
                        As seen briefly in the video, our user interface greets
                        you with a table that looks as so:
                            
                        @image html TPUI.JPG
                        
                        This encases all of our user interface's relevant functionality.
                        These functions allow you to effectively use the ball
                        balancing platform and play around with it enough to 
                        get it working while getting an understanding of what's
                        going on in the system itself.
                        
                        A breakdown of this functionality is as follows:
                            
                        p/P - Retrieve the euler angles from the IMU and display
                              them in the user interface as [thx, thy, thz] in degrees.
                              
                        v/V - Retrieve the angular velocities from the IMU and display
                              them in the user interface as [wx, wy, wz] in degrees per second.
                              
                         c  - Starts touchpad calibration. If there is already
                              a file with touch panel calibration coefficients
                              named "RT_cal_coeffs.txt" then they will be loaded.
                              If not the user will be guided through the calibration process.
                              
                         q  - Prints the touch panel readings, both positions and
                              velocities.
                              
                         g  - Lets the user change both duty cycle offsets to
                              account for imbalance in the system and motor bias.
                              
                        k/K - Lets the user input all four gains needed for closed
                              loop control.
                              
                        w/W - Lets the user toggle closed loop control mode. Starts
                              turned off and then user must enable it for balancing.
                              
                        x/X - Takes x-data for 15 seconds, meaning that the x position
                              and velocity as well as the angle and angular velocity about
                              the y-axis. After collecting, the program will be blocked
                              as the data is written to a file.
                              
                        y/Y - Takes y-data for 15 seconds, meaning that the y position
                              and velocity as well as the angle and angular velocity about
                              the x-axis. After collecting, the program will be blocked
                              as the data is written to a file.
                              
                        b/B - Takes x and y positions for 15 seconds. Like the
                              other data collection methods, this will block the
                              program as the file is written afterwards.
                              
                        s/S - Exit data collection modes or the user interface
                              altogether.
                        
    @author             Sean Wahl
    @author             Grant Gabrielson
    @author             Spencer Alonge
    @date               March 16, 2022
'''