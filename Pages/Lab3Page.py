'''! @page Lab3Page Lab 3 - DC Motor Control
    @section sec_gen    Overview
                        In Lab 3, we figured out how to control motor speeds
                        using pulse-width modulation. We then added this new
                        ability to a motor task run by a modified user task from
                        @ref Lab2Page with additional functionality. The encoder
                        values are now read in radians, and the user has increased
                        inputs to be able to access all of the functionality
                        of our code. All of this was run from a main file. 

                        A brief description of all files used are listed below:
                            
                        Lab3Main.py - Calls the function files and creates 
                                      shared variables used to communicate
                                      between the files. Runs the tasks by
                                      calling for the next input from each.
                                      
                        taskUser2.py - Creates a user interface and handles all
                                      user input. Also does all the printing
                                      of data from taskEncoder2.
                                      
                        taskEncoder2.py - Sets up an Encoder object using the 
                                         encoder class. Transitions between 
                                         states based on what taskUser2 tells it
                                         to do.
                                         
                        @ref encoder.py - Defines an encoder class with several
                                     functions. Will initialize an encoder if
                                     given proper inputs, and lets the encoder
                                     update, return its position, return its 
                                     last change in position, and zero itself.
                                     See encoder.Encoder for details. All returned
                                     values are radian based.
                            
                        taskMotor.py - Sets up a motor driver and motor objects.
                                       Is able to set motor duty cycles based
                                       on user input taken in taskUser2.
                                     
                        motor.py - Defines a motor class with the ability to change
                                   motor duty cycles through pulse-width modulation.
                                   Needs pin objects corresponding to the desired
                                   motor and a timer object with correct timer
                                   channels. See motor.Motor for details.
                                   
                        @ref DRV8847.py - Defines a motor driver class with the ability
                                     to set up motors under a common timer, sleep
                                     pin, and fault pin. Allows for motor enabling
                                     and disabling along with fault detection.
                                     See DRV8847.DRV8847 for details.
                                     
                        @ref shares.py - Sets up a sharing class in order to create
                                    variables accessible from multiple files.
                                    Also can create queues, however this was not
                                    utilized here. Provided courtesy of Charlie
                                    Refvem at Cal Poly.
                        
    @section sec_STDs   State Transition Diagrams
                        The finite state machines for the generator functions
                        are very important as they represent the logic behind
                        the code we've written.
                        
                        For the taskEncoder2.py function, the finite state machine
                        looks like:
                            
                        @image html Lab3EncoderStates.JPG
                        
                        For the taskMotor.py function, the finite state machine
                        is as follows:
                            
                        @image html Lab3MotorStates.JPG
                        
                        For the taskUser2.py function, the finite state machine is:  
                            
                        @image html Lab3UserStates.JPG
                        
                        
                        
    @section sec_ODs   Other Diagrams
                        In addition to the finite state machines we also made a
                        few other diagrams to aid in understanding our code's 
                        structure.
                        
                        The Task Diagram for the main file is as pictured below:
                            
                        @image html Lab3TD.JPG
                        
                        The Encoder Class diagram is:
                            
                        @image html Lab3EncoderClass.JPG
                        
                        The Motor Class diagram is:
                            
                        @image html Lab3MotorClass.JPG
                        
                        The DRV8847 Class diagram is:
                            
                        @image html Lab3DRVClass.JPG
                        
    @section sec_plot  Data Collection Plots
                        After running our code and collecting data for the
                        full 30 seconds, we were able to create the following
                        plot. Note: for this data collection the motor was run
                        at a duty cycle of 20%. Since the max motor speed is around
                        180 r/s, this corresponds to a speed of about 30 r/s.
                        
                        @image html Lab3PosVTime.JPG
                        
                        For duty cycle collection data we found that the measured
                        speeds agreed very well with the expected speeds, differing
                        just ever so slightly.
                        
                        @image html Lab3SpeedVDC.JPG
                        
    @author             Sean Wahl
    @author             Grant Gabrielson
    @date               February 16, 2022
'''