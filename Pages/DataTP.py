'''! @page DataTP   Term Project Data Collection
    @section sec_dathom  Data Collection Reasoning
                        Our term project has a task specifically designated for
                        data collection, taskData.py. A brief demo of the data
                        collection through the user interface can be found in
                        the demonstration in @ref DemonstrationTP. 
                        
                        It's useful to collect the data so that we can be certain
                        that our methods are working in the right ways, as in
                        we didn't just get lucky when the ball balanced. This had
                        happened to us before, where we had the ball balancing but
                        our touch panel data didn't make any sense so we had to
                        redo that part of the project and then re-tune everything.
                        
                        It is also useful to analyze the trends in the ball's
                        and platform's motion during the balancing protocol, and
                        then we can reason why things turned out how they did.
                       
                        taskData had three data sets that it could collect, so in
                        this page we will go over each set and then what the data
                        it collects looks like.
                        
    @section sec_posplots  Position Data
                        The first data set that we can collect is x and y position
                        of the ball on the touch panel over time. This is neat
                        to look at because it maps out all positions of the ball
                        during the balancing protocol, and there's a couple of
                        cool ways to plot this.
                        
                        The first way we plotted this position data is with a
                        3-D plot, with x and y on the horizontal axes and time
                        going upwards.
                        
                        @image html TPPlot1.JPG
                        
                        This graph looks like a cyclone, demonstrating the ball's
                        tendency to circle around the center of the platform. This
                        plot can be a little difficult to look at without being
                        able to manipulate it, so we also made an animation.
                        
                        @image html BallPosition.gif
                        
                        This also shows the ball's tendency to swirl about the
                        center but it is easier to digest this information when
                        the third axis is taken out.
                        
                        
    @section sec_xdat  X-Related Data
                        Another data collection mode we implemented was to take
                        all data relating to the x-position axis on the panel.
                        This means the position and velocity along the x-axis along
                        with the angle and angular velocity about the y-axis. The
                        reason we took data like this was since the rotation about
                        the y-axis affected the ball's movement along the x-axis.
                        
                        After taking this data, we can make four plots like we
                        did in homework 3. This allows us to see how the state
                        variables affect one another.
                        
                        @image html TPPlot2.JPG
                        
                        It's a little hard to see because of the time scale, but
                        we can see that when the ball is on the negative x side
                        of the board, the closed loop controller tries to bring it
                        towards the center with a positive y-angle. This causes
                        a positive x-velocity. This is in accordance to how we
                        thought it would behave.
                        
    @section sec_ydat  Y-Related Data
                        Similarly to the last section, our final data collection
                        mode is to tak all data relating to the y-position axis
                        on the panel. This meant the position and velocity of the
                        ball on the y-axis, and the angle and angular velocity
                        about the x-axis. Again, this is because a rotation about
                        the x-axis causes movement along the y-axis.
                        
                        @image html TPPlot3.JPG
                        
                        Again, we see the same trends as the last section, giving
                        us confidence that our balancing protocol works as
                        intended.
                        
                        Though, one thing to note about this set of plots is that
                        the data seems to be much more erratic. We believe this
                        is because the ball has less room to move on the y-axis, 
                        so the control has to be more aggressive in this direction. 
                        While that makes sense, we didn't program this feature into
                        our code, so it could also just be that the motor controlling
                        this rotation was weaker or at least more sensitive. Nevertheless, 
                        the ball still balanced so we think whatever is happening 
                        is still okay.
    @author             Sean Wahl
    @author             Grant Gabrielson
    @author             Spencer Alonge
    @date               March 16, 2022
'''
