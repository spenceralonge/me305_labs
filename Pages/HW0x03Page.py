'''! @page HW0x03       Ball-Balancing Platform Simulation
    @section sec_gen    Overview
                        For Homework 0x03, we were tasked with using our equations
                        of motion that we derived in Homework 0x02 and manipulating
                        them to be in a form that we can easily simulate. The steps
                        we used to do so are as follows based on the homework
                        assignment:
                            
                            1. Decouple the system of equations by multiplying
                               M^-1*f
                            2. Convert the system of two second-order ODEs to
                               a system of four first-order ODEs
                            3. Determine equilibrium points at which the system
                               will remain stationary
                            4. Find the two Jacobian matrices for the new system
                               based on the new state vector and the input torque
                            5. Calculate matrices A and B by evaluating the Jacobians
                               at the equilibrium points
                               
                        After completing this, we had equations of motion representing
                        x, xdot, theta, and thetadot for a given torque meaning 
                        we could create an ODE in MATLAB that we can simulate 
                        when given a time span and initial conditions.
                        
                        The following is the block diagram used in simulating
                        the system.
                        
                        @image html 0x03Sim
                       
                        
    @section sec_MAT    MATLAB Code
                        Here is the fully documented MATLAB script used to
                        simulate the system.
                        \htmlonly
                        <center> <embed src="Alonge0x03.pdf" width="850px" height="1000px" href="HW0x03.pdf"></embed> </center>
                        \endhtmlonly
                        
    @author             Sean Wahl
    @author             Spencer Alonge
    @date               February 16, 2022
'''