'''! @page GeneralTP Term Project Bookkeeping
    @section sec_gen    Overview
                        We had to build off of all our existing knowledge as well
                        as add to it to get our term project working. Here
                        will be everything related to the files of the term
                        project, like how regular labs were presented.
        
                        
                        A brief description of all files used are listed below:
                            
                        TPMain.py - Calls the function files and creates 
                                      shared variables used to communicate
                                      between the files. Runs the tasks by
                                      calling for the next input from each.
                                      
                        taskUserTP.py - Creates a user interface and handles all
                                      user input. Can retrieve and send data from and
                                      to the other tasks.
                                      
                        taskBNOTP.py - Uses the IMU class in order to calibrate
                                     the IMU and then constantly retrieve euler
                                     angles and angular velocities from the platform.
                                         
                        BNO055TP.py - Sets up an IMU using I2C communication and
                                     gives it several important methods that
                                     relate to calibrating and retrieving data
                                     from the chip. See BNO055TP.BNO055 for details.
                            
                        taskMotorTP.py - Sets up a motor driver and motor objects.
                                       Is able to set motor duty cycles based
                                       on what's calculated by taskClosedLoop.
                                     
                        motorTP.py - Defines a motor class with the ability to change
                                   motor duty cycles through pulse-width modulation.
                                   Needs pin objects corresponding to the desired
                                   motor and a timer object with correct timer
                                   channels. See motorTP.Motor for details.
                                     
                        taskClosedLoopTP.py - Sets up a motor controller object and
                                    calculates appropriate duty cycles (torques)
                                    to level the platform.
                                    
                        closedloopTP.py - Defines a motor controller for closed loop
                                    motor control. Is able to do the calculations
                                    that taskClosedLoop returns. See closedloopTP.ClosedLoop
                                    for details.
                        
                        taskPanel.py - Sets up a touch panel object that can
                                    be calibrated by the user. Continously
                                    retrieves positions from the touch panel and
                                    filters them, as well as calculates velocity
                                    estimates.
                                    
                        panel.py - Defines a touch panel object class. This class
                                    has various methods of scanning for positions
                                    and contact, and can have it's positional
                                    calibration coefficients changed to remap
                                    the axes. See panel.Panel for details.
                                    
                        taskData.py - Sets up a data collection task that will
                                    take a user specified set of data for 15
                                    seconds. Afterwards, the code becomes blocking
                                    but will write to a file quickly.
                        
                        @ref shares.py - Sets up a sharing class in order to create
                                    variables accessible from multiple files.
                                    Also can create queues, however this was not
                                    utilized here. Provided courtesy of Charlie
                                    Refvem at Cal Poly.
                        
    @section sec_STDs   Finite State Machines
                        The finite state machines for the generator functions
                        are very important as they represent the logic behind
                        the code we've written.
                        
                        For the taskBNOTP.py function, the finite state machine
                        looks like:
                            
                        @image html TPBNOStates.JPG
                        
                        For the taskMotorTP.py function, the finite state machine
                        is as follows:
                            
                        @image html TPMotorStates.JPG
                        
                        For the taskClosedLoopTP.py function, the finite state machine
                        is:
                            
                        @image html TPCLCStates.JPG
                        
                        For the taskUserTP.py function, the finite state machine is:  
                            
                        @image html TPUserStates.JPG
                        
                        For the taskPanel.py function, the finite state machine is:
                            
                        @image html TPPanelStates.JPG
                        
                        Lastly, for the taskData.py function, the finite state machine is:
                            
                        @image html TPDataStates.JPG
                        
     @section sec_ODs    Other Diagrams
                        In addition to the finite state machines we also made a
                        task diagram to showcase the overall communication
                        between tasks. The Task Diagram for the main file is as 
                        pictured below:
                            
                        @image html TPTD.JPG
                        
                        We also made several class diagrams, one for each class:
                            
                        @image html TPMotorClass.JPG
                        @image html TPBNOClass.JPG
                        @image html TPCLCClass.JPG
                        @image html TPPanelClass.JPG
                    
    @author             Sean Wahl
    @author             Grant Gabrielson
    @author             Spencer Alonge
    @date               March 16, 2022
'''