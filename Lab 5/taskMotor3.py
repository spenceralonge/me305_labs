'''!@file           taskMotor3.py
    @brief          A file to interact and communicate with motors.
    @details        By using taskUser to input values, taskMotor is able
                    to set duty cycles on the individual motors it controls.
                    This small subset of motor functionality only requires three states:
                    State 0 intitializes the driver and motors, State 1 constantly
                    tries to set motor duty cycles. State 3 is for closed loop
                    control and sets the duty cycles of the motors as calculated
                    by the closed loop control task in order to obtain proper
                    torques to level the platform. Reference the finite state
                    machine as needed:
                        
                    @image html Lab5MotorStates.JPG
                    
                    As always, feel free to reference the source code at
                    https://bitbucket.org/seanwahl2023/me_305_labs/src/master/Lab%205/
                    however especially in this case since function variables
                    are not captured by Doxygen.
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           February 23, 2022
'''

import micropython
from motor3 import Motor
from pyb import Pin, Timer
from time import ticks_us, ticks_add, ticks_diff

def taskMotorFunction(taskName, period, DC1, DC2, wFlag, L1, L2):
    '''!@brief              Motor control task.
        @details            taskMotor constantly tries to update the duty cycles
                            of motors 1 and 2. It runs on a set period and will
                            only run at times that meet this period. It only has
                            three states, one of which intiializes motors. 
                            State 1 is where the task sets the DC values to 
                            whatever taskUser has designated. State 2 is entered 
                            through user input and is where closed loop control
                            to balance the platform occurs. State 1 and State 2
                            can easily be toggled between.
        @param taskName     The arbitrary chosen name of the task.
        @param period       The time interval at which this task should operate.
        @param DC1          A shared variable holding the duty cycle of motor 1.
        @param DC2          A shared variable holding the duty cycle of motor 2.
        @param err          A shared variable indicating an fault has been detected.
        @param wFlag        A shared variable indicating when the closed loop control should be active.
        @param L1           A shared variable holding the required duty cycle 
                            of motor 1 as calculated by taskClosedLoop.
        @param L1           A shared variable holding the required duty cycle 
                            of motor 2 as calculated by taskClosedLoop.
    '''
    
    ## @brief Represents the initialization state of the task.
    #
    S0_INIT = micropython.const(0)
    
    ## @brief Represents the duty cycle setting state of the task.
    #
    S1_UPD = micropython.const(1)
    
    ## @brief Represents the closed loop control state of the task.
    #
    S2_CLC = micropython.const(2)

    
    ## @brief       Denotes the state that the task is currently running in.
    #  @details     taskMotor has three possible states. Reference the taskMotor3.py
    #               file for additional details.
    #
    state = S0_INIT
    
    ## @brief The initial time at which the task has first started running.
    #
    start_time = ticks_us()
    
    ## @brief       The next time the task should run.
    #  @details     By using the set period input, next_time is a calculated
    #               value of when the task should run again in order to be
    #               operating at said period.
    #
    next_time = ticks_add(start_time, period)
    
    while True:
        ## @brief The current time when the function tries to run again.
        #
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:    
            
            next_time = ticks_add(next_time, period)
            
            # Set up motor driver and motor objects.
            if state == S0_INIT:
                ## @brief The timer on which the motor driver will run.
                #
                tim = Timer(3, freq = 20000)
                
                ## @brief Object for motor 1.
                #
                #Motor 2 moves in positive y-axis
                motor_2 = Motor(tim, Pin.cpu.B4, Pin.cpu.B5, 1, 2)
                
                ## @brief Object for motor 2.
                #
                #Motor 1 moves in positive x-axis
                motor_1 = Motor(tim, Pin.cpu.B0, Pin.cpu.B1, 3, 4)

                state = S1_UPD
                yield None
            
            # Constantly update the motor duty cycles.
            elif state == S1_UPD:
                # Check for an enable request.
                if wFlag.read():
                    state = S2_CLC
                    yield None
                    
                # Set duty cycles.
                else:
                    motor_1.set_duty(DC1.read())
                    motor_2.set_duty(DC2.read())
                    yield None
            
            # Allows for closed loop control when wFlag is on.
            elif state == S2_CLC:
                if not wFlag.read():
                    state = S1_UPD
                    yield None
                    
                else:
                    motor_1.set_duty(L1.read())
                    motor_2.set_duty(L2.read())
                    yield None
                    
            # Just in case
            else:
                print('something went wrong')
                break
        else:
            yield None