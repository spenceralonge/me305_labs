'''!@file           taskClosedLoop2.py
    @brief          A file to set closed loop torques (duty cycles) for the motors.
    @details        This file sets up a task to perform motor control during
                    closed loop control mode. It constantly calculates what the
                    duty cycle of the motor should be to reach a value that
                    allows for a platform to be balanced. It does this with user 
                    input gains and values measured from an IMU. It has only 
                    two states. State 1 initializes the motor controller and 
                    then goes to State 1. State 1 constantly updates the user 
                    set values in case of changes and then calculates the 
                    necessary duty cycle and outputs it to the user via a 
                    shared variable. For a state transition diagram, reference the following:
                        
                    @image html Lab5CLCStates.JPG
                    
                    As always, feel free to reference the source code at
                    https://bitbucket.org/seanwahl2023/me_305_labs/src/master/Lab%205/
                    however especially in this case since function variables
                    are not captured by Doxygen.
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           March 3, 2022
'''

import micropython, closedloop2
from time import ticks_us, ticks_add, ticks_diff

def taskClosedLoopFunction(taskName, period, L1, L2, Kp, Kd, Ki, wMeas1, wMeas2, thMeas1, thMeas2):
    '''!@brief              Routinely calculates the necessary torques (duty cycles) for the motors.
        @details            In this closed loop control, the controller takes in the
                            current rotation of the platform and its angular velocities
                            in order to figure out proper duty cycles to apply
                            to the motors to bring the platform back to level.
                            We do this with gains that allow tuning of the platform's
                            response. Proportional gain acts on the error in the
                            angles of the platform, derivative gain acts on the
                            error in the angular velocities of the platform, and
                            integral gain acts on the systematic error in the
                            platform in bringing it to level. The states necessary
                            to perform this are a initialization state to set
                            up the controllers, and then an update state to constantly
                            calculate what the duty cycles of the motors should be.
        @param taskName     The arbitrary, chosen name of the task.
        @param period       The time interval at which this task should operate.
        @param L1           A shared variable for the duty cycle (Torque) of the motor 1.
        @param L2           A shared variable for the duty cycle (Torque) of the motor 2.
        @param Kp           A shared variable holding the user input Kp gain.
        @param Kd           A shared variable holding the user input Kd gain.
        @param Ki           A shared variable holding the user input Ki gain.
        @param wMeas1       A shared variable holding the IMU read platform 
                            angular velocity about the x-axis.
        @param wMeas2       A shared variable holding the IMU read platform 
                            angular velocity about the y-axis.
        @param thMeas1      A shared variable holding the IMU read platform 
                            euler angle about the x-axis.
        @param thMeas2      A shared variable holding the IMU read platform 
                            euler angle about the y-axis.
    '''
    
    ## @brief Represents the initialization state of the task.
    #
    S0_INIT = micropython.const(0)
    
    ## @brief Represents the updating state of the task.
    #
    S1_UPD = micropython.const(1)
    
    ## @brief       Denotes the state that the task is currently running in.
    #  @details     taskClosedLoop has two possible states. State 0 is the
    #               initialization state and sets up the closed loop controllers
    #               necessary. State 1 then constantly calculates the duty
    #               cycles needed by the motors.
    #
    state = S0_INIT
    
    ## @brief The initial time at which the task has first started running.
    #
    start_time = ticks_us()
    
    ## @brief       The next time the task should run.
    #  @details     By using the set period input, next_time is a calculated
    #               value of when the task should run again in order to be
    #               operating at said period.
    #
    next_time = ticks_add(start_time, period)

    
    while True:
        ## @brief The current time when the function tries to run again.
        #
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:    
            
            next_time = ticks_add(next_time, period)
            
            # Sets up motor controller object.
            if state == S0_INIT:
                ## @brief The motor controller object for motor 1 (x-axis rotation)
                #
                controller = closedloop2.ClosedLoop()
                
                ## @brief The motor controller object for motor 2 (y-axis rotation)
                #
                controller2 = closedloop2.ClosedLoop()
                state = S1_UPD
                yield None
            
            # Constantly updates the gains and required duty cycle
            elif state == S1_UPD:
                controller.set_gain(Kp.read(), Kd.read(), Ki.read())
                controller2.set_gain(Kp.read(), Kd.read(), Ki.read())
                L1.write(controller.run(thMeas1.read(), wMeas1.read()))
                L2.write(controller2.run(thMeas2.read(), wMeas2.read()))
            
            # Just in case
            else:
                print('something went wrong')
                break
        else:
            yield None
