'''!@file           taskBNO.py
    @brief          A file to calibrate and retrieve values from an IMU.
    @details        This file sets up a task to calibrate the IMU and then
                    constantly retrieve euler angles and angular velocities from
                    it. Upon startup, it checks for a calibration coefficient file
                    and will calibrate off of that if present. If not, it will
                    wait until all calibration statuses are 3 (besides the system
                    one as it is inconsistent) and then create a calibration
                    file for future use. Then it will be constantly retrieving
                    euler angles and angular velocities. For a task diagram
                    please consult the image below:
                    
                        
                    @image html Lab5BNOStates.JPG
                    
                    As always, feel free to reference the source code at
                    https://bitbucket.org/seanwahl2023/me_305_labs/src/master/Lab%205/
                    however especially in this case since function variables
                    are not captured by Doxygen.
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           March 3, 2022
'''

import micropython
from BNO055 import BNO055
import os
from time import ticks_us, ticks_add, ticks_diff

def taskBNOFunction(taskName, period, cal_state, velocity, euler, calFlag, wMeas1, wMeas2, thMeas1, thMeas2, fileFlag):
    '''!@brief              Constantly retrieves values from the IMU.
        @details            This generator function, given the right set of
                            inputs, is able to constantly retrieve euler angles
                            and angular velocities from the IMU. Upon startup
                            it goes through a calibration protocol in which
                            either calibration coefficients are loaded from an
                            existing file or the IMU stays in calibration mode
                            until all system variables are 3. After this, it
                            enters an update mode where the IMU values are
                            continuously retrieved til the end of time.
        @param taskName     The arbitrary, chosen name of the task.
        @param period       The time interval at which this task should operate.
        @param cal_state    A shared variable holding the calibration status of the IMU.
        @param velocity     A shared variable that holds the current angular velocities.
        @param euler        A shared variable that holds the current euler angles.
        @param calFlag      A shared variable that designates when calibration
                            is complete.
        @param wMeas1       A shared variable that holds the angular velocity 
                            about the x-axis.
        @param wMeas2       A shared variable that holds the angular velocity 
                            about the y-axis.
        @param thMeas1      A shared variable that holds the euler angle about the
                            x-axis.
        @param thMeas2      A shared variable that holds the euler angle about the
                            y-axis.
        @param fileFlag     A shared variable indicating that a calibration file has
                            been read from.
    '''
    
    ## @brief Represents the initialization state of the task.
    #
    S0_INIT = micropython.const(0)
    
    ## @brief Represents the calibration state of the task.
    #
    S1_CALIB = micropython.const(1)    
    
    ## @brief Represents the main state of the task in which values are retrieved.
    #
    S2_UPD = micropython.const(2)
    
    ## @brief       Denotes the state that the task is currently running in.
    #  @details     taskBNO has 4 possible states. Please see the taskBNO.py
    #               file for details on the states.
    #
    state = S0_INIT
    
    ## @brief Represents when the task first runs.
    #
    start_time = ticks_us()
    
    ## @brief The time at which the task should run again.
    #
    next_time = ticks_add(start_time, period)
    
    while True:
        ## @brief The current time when the function tries to run again.
        #
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:    
            
            next_time = ticks_add(next_time, period)
            
            # 
            if state == S0_INIT:
                ## The IMU driver object.
                #
                driver = BNO055()
                
                _path = os.getcwd()
                _dir_list = os.listdir(_path)
                if 'IMU_cal_coeffs.txt' in _dir_list:
                    driver.cal_write('IMU_cal_coeffs.txt')
                    fileFlag.write(True)
                    state = S2_UPD
                    yield None
                else:
                    state = S1_CALIB
                    driver.op('NDOF')
                    yield None
                    
            elif state == S1_CALIB:
                cal_state.write(driver.cal_status())
                if driver.cal_status() == [0,3,3,3]: 
                    driver.cal_read('IMU_cal_coeffs.txt')
                    state = S2_UPD
                    calFlag.write(True)
                    yield None
                else:
                    yield None
                    
            elif state == S2_UPD:
                thMeas1.write(driver.euler()[0])
                thMeas2.write(driver.euler()[1])
                wMeas1.write(driver.ang_vel()[0])
                wMeas2.write(driver.ang_vel()[1])
                euler.write(driver.euler())
                velocity.write(driver.ang_vel())
                yield None
            
            
                    
            # Just in case
            else:
                print('something went wrong')
                break
        else:
            yield None
    
    
    