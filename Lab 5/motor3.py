'''!@file           motor3.py
    @brief          Creates a motor class that allows duty cycles to be set.
    @details        The motor class takes in timer and pin objects as well as
                    timer channels. These inputs enable it to be set up such
                    that the pins can be set with pulse-width modulation to
                    control the motor duty cycles. These pin settings were obtained
                    from their proper motor data sheets. Once set up, the only
                    method available within the class is set_duty, allowing
                    the motor speed to be set on a percent basis.
                    
                    For a class diagram, reference the following:
                        
                    @image html Lab5MotorClass.JPG
                    
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           February 16, 2022
'''

from pyb import Pin, Timer

class Motor:
    '''!@brief               Creates an motor object that can set duty cycles.
        @details             A motor object can be created by calling the motor
                             class with pin and timer related inputs. Once
                             designated, a motor object is able to control the
                             duty cycles through pulse-width modulation, setting
                             the percent of the max speed that the motor will
                             spin at.
        @author              Sean Wahl
        @author              Grant Gabrielson
        @author              Spencer Alonge
        @date                February 16, 2022
    '''
    
    def __init__ (self, PWM_tim, IN1_pin, IN2_pin, ch1, ch2):
        '''!@brief      Initialize the motor object.
            @details    Using a timer, 2 pin objects corresponding to the motor
                        control pins, and 2 channels corresponding to the timer
                        channels, a motor object can be initialized for use.
            @param PWM_tim  Timer used to control the motors.
            @param IN1_pin  Pin 1 hooked up to the motor.
            @param IN2_pin  Pin 2 hooked up to the motor.
            @param ch1      Timer channel 1 used by the motor.
            @param ch2      Timer channel 2 used by the motor.
        '''
        self._pin1 = Pin(IN1_pin, Pin.OUT_PP)
        self._pin2 = Pin(IN2_pin, Pin.OUT_PP)
        self._ch1 = PWM_tim.channel(ch1, Timer.PWM_INVERTED, pin = self._pin1)
        self._ch2 = PWM_tim.channel(ch2, Timer.PWM_INVERTED, pin = self._pin2)
        self._ch1.pulse_width_percent(0)
        self._ch2.pulse_width_percent(0)
        pass
        
    def set_duty (self, duty):
        '''!@brief      Set the duty cycle of the motor.
            @details    Specify a percent of max motor speed from -100 to 100
                        for the motor to spin at. Negative values cause backwards
                        rotation of the motor.
            @param duty Duty cycle at which the motor should operate.
        '''
            #forward
        if duty >= 0:
            self._ch2.pulse_width_percent(0)
            self._ch1.pulse_width_percent(duty)
            
            #backward
        elif duty < 0:         
            self._ch1.pulse_width_percent(0)
            self._ch2.pulse_width_percent(-duty)
        pass

