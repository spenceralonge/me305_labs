'''!@file           taskUser4.py
    @brief          A file to take user input and communicate with the other tasks.
    @details        taskUser sets up a generator function to run on a specified
                    period. This function takes in user specified values and
                    performs the designated operation for their input. It also
                    can send or retrieve values from taskBNO, taskMotor, and
                    taskClosedLoop in order to create a well-oiled machine for
                    motor control. taskUser is like the control task for the system. For a
                    breakdown of the states, please check out the function details.
                    For a more comprehensive understanding of the state transitions,
                    reference the following diagram:
                        
                    @image html Lab5UserStates.JPG
                    
                    As always, feel free to reference the source code at
                    https://bitbucket.org/seanwahl2023/me_305_labs/src/master/Lab%205/
                    however especially in this case since function variables
                    are not captured by Doxygen.
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           March 3, 2022
'''

from time import ticks_us, ticks_add, ticks_diff
import pyb, micropython

def taskUserFunction(taskName, period, velocity, DC1, DC2, L1, L2, Kp, Kd, Ki, wMeas1, wMeas2, thMeas1, thMeas2, wFlag, euler, cal_state, calFlag, fileFlag):
    '''!@brief              Routinely checks for user input and acts accordingly.
        @details            This generator function, given the right set of
                            inputs, is able to constantly check for user input
                            and will change state based on what the user specifies.
                            It reads shared variables that taskBNO, taskMotor,
                            and taskClosedLoop write to in order to relay data
                            to the user. It can also write to these variables
                            to give user info to the tasks themselves. There are 
                            8 various states that taskUser can operate in. 
                            Please just reference the Lab 4 page or the taskUser4.py 
                            file for the state transition diagram documenting all 
                            the states.
        @param taskName     The arbitrary, chosen name of the task.
        @param period       The time interval at which this task should operate.
        @param velocity     A shared variable that holds the angular velocity
                            of the platform, recorded by the IMU.
        @param DC1          A shared variable that holds the duty cycle for motor 1.
        @param DC2          A shared variable that holds the duty cycle for motor 2.
        @param L1           A shared variable holding the required duty cycle
                            (torque) for motor 1.
        @param L2           A shared variable holding the required duty cycle
                            (torque) for motor 2.
        @param Kp           A shared variable holding the user input Kp gain.
        @param Kd           A shared variable holding the user input Kd gain.
        @param Ki           A shared variable holding the user input Ki gain.
        @param wMeas1       A shared variable holding the measured angular
                            velocity of the platform about the x-axis.
        @param wMeas2       A shared variable holding the measured angular
                            velocity of the platform about the y-axis.
        @param thMeas1      A shared variable holding the measured euler
                            angle of the platform about the x-axis.
        @param thMeas2      A shared variable holding the measured euler
                            angle of the platform about the y-axis.
        @param wFlag        A shared variable indicating when closed-loop control
                            mode should be engaged.
        @param euler        A shared variable that holds the euler angles of the
                            platform, recorded by the IMU.
        @param cal_state    A shared variable holding the calibration status
                            of the IMU components.
        @param calFlag      A shared variable indicating when the IMU is calibrated.
        @param fileFlag     A shared variable indicating when a calibration
                            file has been used to calibrated the IMU.
    '''
    
    ## @brief Represents the initialization state of the task.
    #
    S0_INIT = micropython.const(0)
    
    ## @brief Represents the command center state of the task.
    #
    S1_CMD = micropython.const(1)
    
    ## @brief Represents the state in which the IMU is being calibrated.
    #
    S2_CALIB = micropython.const(2)
    
    ## @brief Represents the duty cycle specifying state.
    #
    S3_DUTY = micropython.const(3)
    
    ## @brief Represents the Kp setting state.
    #
    S4_KP = micropython.const(9)
    
    ## @brief Represents the Kd setting state.
    #
    S5_KD = micropython.const(10)
    
    ## @brief Represents the Ki setting state.
    #
    S6_KI = micropython.const(12)
    
    ## @brief Represents the state in which numerical values are read.
    #
    S7_READ = micropython.const(11)
    
    ## @brief       Denotes the state that the task is currently running in.
    #  @details     taskUser has 8 possible states. Please see the taskUser4.py
    #               file for details on the states.
    #
    state = S0_INIT
    
    ## @brief Contains serial inputs from the keyboard.
    #
    serport = pyb.USB_VCP()
    
    ## @brief The time at which the task is first run.
    #
    start_time = ticks_us()
    
    ## @brief The time at which the task should start its next cycle.
    #
    next_time = ticks_add(start_time, period)
    
    while True:
        ## @brief The time at the beginning of the current task run.
        #
        current_time = ticks_us()
        
        # Check if alarm clock went off
        if ticks_diff(current_time, next_time) >= 0:
            next_time = ticks_add(next_time, period)
            
            # Initialize
            if state == S0_INIT:
                ## @brief The string of numbers input during the number reading state.
                #
                num = ''
                state = S2_CALIB
                
            # Command Center
            elif state == S1_CMD:
                if serport.any():
                    ## @brief Character data from user's keyboard inputs.
                    #
                    charIn = serport.read(1).decode()
                    
                    #Set state according to user input
                    if charIn in {'p','P'}:
                        print(f"The platform's euler angles are {euler.read()} [degrees]")
                        yield None
                        
                    elif charIn in {'v','V'}:
                        print(f"The platform's angular velocities are {velocity.read()} [degrees/s]")
                        yield None
                        
                    elif charIn in {'m','M'}:
                        if charIn == 'm':
                            ## @brief Variable to indicate which motor's DC should
                            #         be set.
                            #
                            motor = 1
                            serport.write('Input duty cycle for motor 1 [%]: ')
                        elif charIn == 'M':
                            motor = 2
                            serport.write('Input duty cycle for motor 2 [%]: ') 
                        ## @brief Indicates which state should be transitioned
                        #         to after the number reading state, State 7.
                        #
                        stateQueue = S3_DUTY 
                        state = S7_READ
                        yield None
                        
                    elif charIn in {'s','S'}:
                        break
                        yield None
                        
                    elif charIn in {'k','K'}:
                        serport.write('Choose closed loop gain Kp [%*s/rad]: ')
                        stateQueue = S4_KP
                        state = S7_READ
                        yield None
                    
                    elif charIn in {'w','W'}:
                        wFlag.write(not wFlag.read())
                        if wFlag.read():
                            print('Closed loop control enabled.')
                        else:
                            print('Closed loop control disabled.')
                        yield None
                    
                    else:
                        print(f'{charIn} is not a valid input. Please try again.')
                        yield None
                    
                else:
                    yield None
            
            elif state == S2_CALIB:
                if fileFlag.read():
                    print('Calibration coefficients read from file.')
                    _printHelp()
                    state = S1_CMD
                    yield None 
                elif calFlag.read():
                    print(f'{cal_state.read()}')
                    print('Calibration complete.')
                    _printHelp()
                    state = S1_CMD
                    yield None
                else:
                    print(f'{cal_state.read()}')
                    yield None
    
            # Read numerical values        
            elif state == S7_READ:
                if serport.any():
                    charIn = serport.read(1).decode()
                    if charIn in {'s','S'}:
                        state = S1_CMD
                        print('Cancelling.')
                    elif charIn.isdigit():
                        num += charIn
                        serport.write(charIn)
                    elif charIn == '-':
                        if len(num) == 0:
                            num += charIn
                            serport.write(charIn)
                        else:
                            yield None
                    elif charIn in {'\x2E', '\x46'}:
                        if charIn not in num:
                            num += charIn
                            serport.write(charIn)
                            yield None
                        else:
                            yield None
                    elif charIn in {'\b','\x08', '\x7F'}:
                        if len(num) == 0:
                            yield None
                        else:
                            num = num[:-1]
                            serport.write('\x7F')
                    elif charIn in {'\r','\n'}:
                        if len(num) == 0:
                            pass
                        else:
                            serport.write('\n \r')
                            ## @brief Holds the user input number as a float.
                            #
                            buf = float(num)
                            num = ''
                            state = stateQueue
                            yield None                            
                else:
                    yield None        
            
            # State to change duty cycle of motor 1 or 2.
            elif state == S3_DUTY:
                if motor == 1:
                    DC1.write(buf)
                    print(f'Motor 1 duty cycle set to {DC1.read()}')
                else:
                    DC2.write(buf)
                    print(f'Motor 2 duty cycle set to {DC2.read()}')
                state = S1_CMD
                yield None
            
            # Closed loop Kp gain setting.
            elif state == S4_KP:
                Kp.write(buf)
                print(f'Kp value set to {Kp.read()} [%*s/rad]')
                serport.write('Choose gain Kd [Hz]: ')
                stateQueue = S5_KD
                state = S7_READ
                yield None
            
            # Closed loop Ki gain setting.
            elif state == S5_KD:
                Kd.write(buf)
                print(f'Kd value set to {Kd.read()} [Hz]')
                serport.write('Choose gain Ki [Hz]: ')
                stateQueue = S6_KI
                state = S7_READ
                yield None
                
            elif state == S6_KI:
                Ki.write(buf)
                print(f'Ki value set to {Ki.read()} [Hz]')
                state = S1_CMD
        else:
            yield None
        
def _printHelp():
    print('+------------------------------------------------------------------+')
    print('|    User Interface:  Please select an input                       |')
    print('+------------------------------------------------------------------+')
    print('| Input |                         Function                         |')
    print('+------------------------------------------------------------------+')
    print('| p / P |  Retrieve euler angles.                                  |')
    print('| v / V |  Retrieve angular velocities.                            |')
    print('|   m   |  Enter a duty cycle (DC) for motor 1.                    |')
    print('|   M   |  Enter a DC for motor 2.                                 |')
    print('| k / K |  Enter a gain for platform closed-loop control.          |')
    print('| w / W |  Enable or disable closed-loop control.                  |')
    print('| s / S |  Exit data collection modes or interface.                |')
    print('|Ctrl+C |  Exit program.                                           |')
    print('+------------------------------------------------------------------+')