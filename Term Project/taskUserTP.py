'''!@file           taskUserTP.py
    @brief          A file to take user input and communicate with the other tasks.
    @details        taskUser sets up a generator function to run on a specified
                    period. This function takes in user specified values and
                    performs the designated operation for their input. It also
                    can send or retrieve values from taskPanel, taskBNO, taskMotor,
                    taskClosedLoop, and taskData. taskUser is essentially the
                    command hub for the entire program, taking in user input and
                    transporting it to our system. 
                    
                    For a more comprehensive understanding of the state transitions,
                    reference the following diagram:
                        
                    @image html TPUserStates.JPG
                    
                    As always, feel free to reference the source code at
                    https://bitbucket.org/seanwahl2023/me_305_labs/src/master/Term%20Project/
                    however especially in this case since function variables
                    are not captured by Doxygen.
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           March 16, 2022
'''

from time import ticks_us, ticks_add, ticks_diff
from ulab import numpy as np
import pyb, micropython

def taskUserFunction(taskName, period, euler, velocity, L1, L2, wFlag, cal_state, calFlag, fileFlag, x, y, xdot, ydot, contact, betamat, touchFlag, Kpthy, Kdthy, Kpx, Kdx, xdataFlag, ydataFlag, posdataFlag, doneFlag, collectionFlag, xgain, ygain):
    '''!@brief              Routinely checks for user input and acts accordingly.
        @details            This generator function, given the right set of
                            inputs, is able to constantly check for user input
                            and will change state based on what the user specifies.
                            It reads shared variables that taskPanel, taskBNO, taskMotor,
                            taskClosedLoop, and taskData write to in order to relay data
                            to the user. It can also write to these variables
                            to give user info to the tasks themselves. There are 
                            7 various states that taskUser can operate in. 
                            Please just reference the Term Project file page or
                            the taskUserTP.py page to see the finite state machine.
        @param taskName     The arbitrary, chosen name of the task.
        @param period       The time interval at which this task should operate.
        @param euler        A shared variable that holds the euler angles of the
                            platform, recorded by the IMU.
        @param velocity     A shared variable that holds the angular velocity
                            of the platform, recorded by the IMU.
        @param L1           A shared variable holding the required duty cycle
                            (torque) for motor 1.
        @param L2           A shared variable holding the required duty cycle
                            (torque) for motor 2.
        @param wFlag        A shared variable indicating when closed-loop control
                            mode should be engaged.
        @param cal_state    A shared variable holding the calibration status
                            of the IMU components.
        @param calFlag      A shared variable indicating when the IMU is calibrated.
        @param fileFlag     A shared variable indicating when a calibration
                            file has been used to calibrate the IMU or the touch panel.
        @param x            A shared variable holding the ball's x-position from the touchpad.
        @param y            A shared variable holding the ball's y-position from the touchpad.
        @param xdot         A shared variable holding the ball's x-velocity from the touchpad.
        @param ydot         A shared variable holding the ball's y-velocity from the touchpad.
        @param contact      A shared variable indicating if the ball is touching the platform.
        @param betamat      A shared variable holding the touch panel calibration coefficients.
        @param touchFlag    A shared variable indicating when the user wants to calibrate the touch panel.
        @param Kpthy        A shared variable holding the closed inner loop proportional gain, acting on the angles of the platform.      
        @param Kdthy        A shared variable holding the closed inner loop derivative gain, acting on the angular velocities of the platform.    
        @param Kpx          A shared variable holding the closed outer loop proportional gain, acting on the position of the ball.    
        @param Kdx          A shared variable holding the closed outer loop derivative gain, acting on the velocity of the ball.           
        @param xdataFlag    A shared variable indicating x-related data should be taken.
        @param ydataFlag    A shared variable indicating y-related data should be taken.
        @param posdataFlag  A shared variable indicating that x and y position data should be taken.
        @param doneFlag     A shared variable indicating when data is done being written to a file.
        @param collectionFlag   A shared variable indicating when data is done being collected.
        @param xgain        A shared variable holding the duty cycle offset needed to make up for motor bias.
        @param ygain        A shared variable holding the duty cycle offset needed to make up for motor bias. 
    '''
    
    ## @brief Represents the initialization state of the task.
    #
    S0_INIT = micropython.const(0)
    
    ## @brief Represents the command center state of the task.
    #
    S1_CMD = micropython.const(1)
    
    ## @brief Represents the state in which the IMU is being calibrated in.
    #
    S2_CALIB = micropython.const(2)
    
    ## @brief Represents the state in which numerical values are read.
    #
    S3_READ = micropython.const(3)
    
    ## @brief Represents the gain setting state.
    #
    S4_K = micropython.const(4)
    
    ## @brief Represents when touch panel values are being recorded for calibration.
    #
    S5_TOUCH = micropython.const(5)
    
    ## @brief Represents the duty cycle offset inputting state.
    #
    S6_OFFSET = micropython.const(6)
       
    ## @brief       Denotes the state that the task is currently running in.
    #  @details     taskUser has 7 possible states. Please see the taskUserTP.py
    #               file for details on the states.
    #
    state = S0_INIT
    
    ## @brief Contains serial inputs from the keyboard.
    #
    serport = pyb.USB_VCP()
    
    ## @brief The time at which the task is first run.
    #
    start_time = ticks_us()
    
    ## @brief The time at which the task should start its next cycle.
    #
    next_time = ticks_add(start_time, period)   
    
    while True:
        ## @brief The time at the beginning of the current task run.
        #
        current_time = ticks_us()
        
        # Check if alarm clock went off
        if ticks_diff(current_time, next_time) >= 0:
            next_time = ticks_add(next_time, period)
            
            # Initialize
            if state == S0_INIT:
                ## @brief The string of numbers input during the number reading state.
                #
                num = ''
                state = S2_CALIB
                ## @brief Indicates what kind of data should be 
                #         printed in the print state.
                
            # Command Center
            elif state == S1_CMD:
                if serport.any():
                    ## @brief Character data from user's keyboard inputs.
                    #
                    charIn = serport.read(1).decode()
                    
                    #Set state according to user input
                    if charIn in {'p','P'}:
                        print(f"The platform's euler angles are {euler.read()} [degrees]\r\n")
                        yield None
                        
                    elif charIn in {'v','V'}:
                        print(f"The platform's angular velocities are {velocity.read()} [degrees/s]\r\n")
                        yield None
                        
                    elif charIn in {'s','S'}:
                        break
                        yield None
                        
                    elif charIn in {'k','K'}:
                        print('Choose desired gains.')
                        serport.write(' Kpth [%/deg]: ')
                        ## @brief Index for setting gain values.
                        #
                        kidx = 1
                        ## @brief Indicates which state should be transitioned
                        #         to after the number reading state, State 7.
                        #
                        stateQueue = S4_K
                        state = S3_READ
                        yield None
                    
                    elif charIn in {'w','W'}:
                        wFlag.write(not wFlag.read())
                        if wFlag.read():
                            print('Closed loop control enabled.\r\n')
                        else:
                            print('Closed loop control disabled.\r\n')
                        yield None
                        
                    elif charIn in {'c','C'}:
                        ## @brief Index for calibrating the touch panel.
                        #
                        touchidx = 0
                        state = S5_TOUCH
                        ## @brief Array for calculating touch panel calibration coefficients.
                        X = np.array([[0,0,1], [0,0,1], [0,0,1], [0,0,1], [0,0,1]])
                        touchFlag.write(True)
                        yield None
                    
                    elif charIn in {'q','Q'}:
                        print(f'Object is at [{x.read()}, {y.read()}] [mm]\r\n')
                        print(f'Velocities are [{xdot.read()}, {ydot.read()}] [mm/s]\r\n')
                        yield None
                        
                    elif charIn in {'l','L'}:
                        print(f'L1: {L1.read()}, L2: {L2.read()}')
                        
                    elif charIn in {'x','X'}:
                        print('Collecting x-related data for around 15 seconds.\r\n')
                        xdataFlag.write(True)
                        yield None
                    
                    elif charIn in {'y','Y'}:
                        print('Collecting y-related data for around 15 seconds.\r\n')
                        ydataFlag.write(True)
                        yield None
                        
                    elif charIn in {'b','B'}:
                        print('Collecting x and y position data for around 15 seconds.\r\n')
                        posdataFlag.write(True)
                        yield None
                        
                    elif charIn in {'g','G'}:
                        print('Choose duty cycle offsets.')
                        serport.write(' xgain [%]: ')
                        stateQueue = S6_OFFSET
                        state = S3_READ
                        ## @brief Index for setting duty cycle offsets.
                        #
                        offidx = 1
                        yield None
                        
                    else:
                        print(f'{charIn} is not a valid input. Please try again.')
                        yield None
                
                elif collectionFlag.read():
                    collectionFlag.write(False)
                    print('Data collected, writing to file. Pausing balancing protocol.\r\n')
                    yield None
                elif doneFlag.read():
                    doneFlag.write(False)
                    print('Data written to file.\r\n')
                else:
                    yield None
            
            elif state == S2_CALIB:
                if fileFlag.read():
                    print('Calibration coefficients read from file.')
                    _printHelp()
                    state = S1_CMD
                    yield None 
                elif calFlag.read():
                    print(f'{cal_state.read()}')
                    print('Calibration complete.')
                    _printHelp()
                    state = S1_CMD
                    yield None
                else:
                    print(f'{cal_state.read()}')
                    yield None
    
            # Read numerical values.     
            elif state == S3_READ:
                if serport.any():
                    charIn = serport.read(1).decode()
                    if charIn in {'s','S'}:
                        state = S1_CMD
                        print('Cancelling.')
                    elif charIn.isdigit():
                        num += charIn
                        serport.write(charIn)
                    elif charIn == '-':
                        if len(num) == 0:
                            num += charIn
                            serport.write(charIn)
                        else:
                            yield None
                    elif charIn in {'\x2E', '\x46'}:
                        if charIn not in num:
                            num += charIn
                            serport.write(charIn)
                            yield None
                        else:
                            yield None
                    elif charIn in {'\b','\x08', '\x7F'}:
                        if len(num) == 0:
                            yield None
                        else:
                            num = num[:-1]
                            serport.write('\x7F')
                    elif charIn in {'\r','\n'}:
                        if len(num) == 0:
                            pass
                        else:
                            serport.write('\r \n')
                            ## @brief Holds the user input number as a float.
                            #
                            buf = float(num)
                            num = ''
                            state = stateQueue
                            yield None                            
                else:
                    yield None        
            
            # Gain setting state.
            elif state == S4_K:
                if kidx == 1:
                    Kpthy.write(buf)
                    serport.write('Kdth [%*s/deg]: ')
                    state = S3_READ
                    kidx += 1
                    yield None
                elif kidx == 2:
                    Kdthy.write(buf)
                    serport.write('Kpx [deg/mm]: ')
                    state = S3_READ
                    kidx += 1
                    yield None
                elif kidx == 3:
                    Kpx.write(buf)
                    serport.write('Kdx [deg*s/mm]: ')
                    state = S3_READ
                    kidx += 1
                elif kidx == 4:
                    Kdx.write(buf)
                    serport.write('\n \r')
                    state = S1_CMD
                yield None
            
            # Touch panel calibration.
            elif state == S5_TOUCH:
                if touchidx == 0:
                    if fileFlag.read():
                        print('Calibration file read, touchpad is ready.\r\n')
                        state = S1_CMD
                        yield None
                    else:
                        _printBoard()
                        print('Touch position 1, press enter to move on.')
                        touchidx += 1
                elif touchidx == 1:
                    if contact.read():
                        X[0,0] = x.read() 
                        X[0,1] = y.read()
                    if serport.any():
                        charIn = serport.read(1).decode()
                        if charIn in {'\r','\n'}:
                            touchidx += 1
                            print(f'{X[0,0]}, {X[0,1]}')
                            print('Touch position 2, press enter to move on.')
                            yield None
                    else:
                        yield None
                elif touchidx == 2:
                    if contact.read():
                        X[1,0] = x.read() 
                        X[1,1] = y.read()
                    if serport.any():
                        charIn = serport.read(1).decode()
                        if charIn in {'\r','\n'}:
                            touchidx += 1
                            print(f'{X[1,0]}, {X[1,1]}')
                            print('Touch position 3, press enter to move on.')
                            yield None
                    else:
                        yield None
                elif touchidx == 3:
                    if contact.read():
                        X[2,0] = x.read() 
                        X[2,1] = y.read()
                    if serport.any():
                        charIn = serport.read(1).decode()
                        if charIn in {'\r','\n'}:
                            touchidx += 1
                            print(f'{X[2,0]}, {X[2,1]}')
                            print('Touch position 4, press enter to move on.')
                            yield None
                    else:
                        yield None
                elif touchidx == 4:
                    if contact.read():
                        X[3,0] = x.read() 
                        X[3,1] = y.read()
                    if serport.any():
                        charIn = serport.read(1).decode()
                        if charIn in {'\r','\n'}:
                            touchidx += 1
                            print(f'{X[3,0]}, {X[3,1]}')
                            print('Touch position 5, press enter to finish.')
                            yield None
                    else:
                        yield None
                elif touchidx == 5:
                    if contact.read():
                        X[4,0] = x.read() 
                        X[4,1] = y.read()
                    if serport.any():
                        charIn = serport.read(1).decode()
                        if charIn in {'\r','\n'}:
                            touchidx = 1
                            print(f'{X[4,0]}, {X[4,1]}')
                            print('Offsets collected, touch panel calibration complete.') 
                            state = S1_CMD
                            ## @brief Array indicating where the touch panel
                            #         calibration points are actually located.
                            Y = [[-80, -40], [-80, 40], [80, 40], [80, -40], [0, 0]]
                            X = np.array(X)
                            Y = np.array(Y)
                            _XT = X.transpose()
                            _mult = np.dot(_XT,X)
                            _INV = np.linalg.inv(_mult)
                            betamat.write(np.dot(np.dot(_INV,_XT),Y))
                            touchFlag.write(False)
                            yield None
                    else:
                        yield None
        
        # Duty cycle offsetting state.       
        elif state == S6_OFFSET:
            if offidx == 1:
                xgain.write(buf)
                state = S3_READ
                serport.write('ygain [%]: ')
                offidx += 1
                yield None
            elif offidx ==2:
                ygain.write(buf)
                state = S1_CMD
                serport.write('\n\r')
                yield None
            yield None    
                
                
        else:
            yield None
        
def _printHelp():
    print('+------------------------------------------------------------------+')
    print('|    User Interface:  Please select an input                       |')
    print('+------------------------------------------------------------------+')
    print('| Input |                         Function                         |')
    print('+------------------------------------------------------------------+')
    print('| p / P |  Retrieve euler angles.                                  |')
    print('| v / V |  Retrieve angular velocities.                            |')
    print('|   c   |  Calibrate the touchpad.                                 |')
    print('|   q   |  Print touchpad reading.                                 |')
    print('|   g   |  Adjust duty cycle offsets.                              |')
    print('| k / K |  Enter a gain for platform closed-loop control.          |')
    print('| w / W |  Enable or disable closed-loop control.                  |')
    print('| x / X |  Take x-axis related data for 15 seconds.                |')
    print('| y / Y |  Take y-axis related data for 15 seconds.                |')
    print('| b / B |  Take x and y position data for 15 seconds.              |')
    print('| s / S |  Exit data collection modes or interface.                |')
    print('|Ctrl+C |  Exit program.                                           |')
    print('+------------------------------------------------------------------+')
    print()
    
def _printBoard():
    print('  2---+---+---+---3')
    print('  |   |   |   |   |')
    print('  +---+---5---+---+')
    print('  |   |   |   |   |')
    print('y 1---+---+---+---4')
    print('|_x                ')