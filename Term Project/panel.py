'''!@file           panel.py
    @brief          A file to set up a touch panel object.
    @details        The panel class takes in pin objects in order to set up the
                    touch panel. These pins should already be connected to the
                    touch panel and allow us to scan in various ways to check
                    x position, y position, and if there's contact or not. Once
                    set up, there are several methods avaiable although all may
                    not be useful, depending on your application.
                    
                    For a class diagram, reference the following:
                        
                    @image html TPPanelClass.JPG
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           March 16, 2022
'''
from pyb import Pin, ADC, Timer
from micropython import const
from time import ticks_us, ticks_diff
import array

class Panel:
    '''!@brief          Creates a touch panel object that can read positions.
        @details        A touch panel object can be created by calling the panel
                        class with 4 pin inputs corresponding to the pins attached
                        to the touch panel physically. Once instantiated, the touch
                        panel is able to read x position, y position, and detect
                        contact in general.
                        
                        By configuring the pins in different ways, we can use
                        ADCs to get a voltage reading from the resistive panel
                        and then convert that to an actual position. This requires
                        calibration coefficients and cycling between pin configurations
                        very quickly.
    
        @author         Sean Wahl
        @author         Grant Gabrielson
        @author         Spencer Alonge
        @date           March 16, 2022
    '''

    def __init__(self, xm, xp, ym, yp):
        '''!@brief          Sets up the touch panel object.  
            @details        With four pin inputs, the touch panel will be set up.
                            In instantiating it, a timer is also set up for
                            oversampling, and several shorthands are defined for
                            future use within the class itself.
            @param xm       Pin hooked up to the negative x side of the board.
            @param xp       Pin hooked up to the positive x side of the board.
            @param ym       Pin hooked up to the negative y side of the board.
            @param yp       Pin hooked up to the positive y side of the board.
        '''
        # ym = pa0, xm = pa1, yp = pa6, xp = pa7
        
        ## @brief Pin hooked up to the negative x side of the board.
        #
        self.Xm = xm
        
        ## @brief Pin hooked up to the positive x side of the board.
        #
        self.Xp = xp
        
        ## @brief Pin hooked up to the negative y side of the board.
        #
        self.Ym = ym
        
        ## @brief Pin hooked up to the positive y side of the board.
        #
        self.Yp = yp
        
        self.calib([1, 0, 0, 1, 0, 0])
        
        # Pin setup shorthands
        self._PO  = const(Pin.OUT_PP)
        self._PI  = const(Pin.IN)
        self._PA  = const(Pin.ANALOG)
        
        # Oversampling timer and buffers.
        self._Tim = Timer(6, freq=100_000)
        self._bufx = array.array('h',32*[0])
        self._bufy = array.array('h',32*[0])
        pass

    def x_scan(self):
        '''!@brief      Retrieve the ADC value in the x-direction.
            @details    Reads the voltage from the resistive touch panel
                        in the x-direction and converts that to an ADC value.
            @return     x-read ADC value, unadjusted.
        '''
        self._ADC = ADC(Pin(self.Yp, self._PA))
        Pin(self.Xm, self._PO).low()
        Pin(self.Xp, self._PO).high()
        Pin(self.Ym, self._PI)
        
        return self._ADC.read()

    def y_scan(self):
        '''!@brief      Retrieve the ADC value in the y-direction.
            @details    Reads the voltage from the resistive touch panel
                        in the y-direction and converts that to an ADC value.
            @return     y-read ADC value, unadjusted.
        '''
        self._ADC = ADC(Pin(self.Xp, self._PA))
        Pin(self.Ym, self._PO).low()
        Pin(self.Yp, self._PO).high()
        Pin(self.Xm, self._PI)
        
        return self._ADC.read()
    
    def z_scan(self):
        '''!@brief      Checks if the board has contact.  
            @details    By doing a special scan, we can check the ADC values
                        in the x and y direction and compare them to what we
                        would expect without contact.
            @return     Logical value indicating whether contact is present.
        '''
        Pin(self.Yp, self.PO).high()
        Pin(self.Xm, self.PO).low()
        self._ADCx = ADC(Pin(self.Xp, self.PA))
        self._ADCy = ADC(Pin(self.Ym, self.PA))
        if (self._ADCx.read() <=10) & (self._ADCy.read() >= 4000):
            return False
        else:
            return True

    def xyz_scan(self):
        '''!@brief      Retrieves the x, y, and contact values from the panel.  
            @details    By scanning in all three ways, we can combine their
                        methods here in order to scan all in one go. This is useful
                        as it is quicker than individually scanning each direction.
                        It also uses the calibration coefficients to adjust their
                        values to fit real data.
            @return     [x,y,z] list containing the x and y positions and the contact state.
        '''
        self._ADCx = ADC(Pin(self.Yp, self._PA))
        Pin(self.Xm, self._PO).low()
        Pin(self.Xp, self._PO).high()
        Pin(self.Ym, self._PI)
        
        self._ADCx.read_timed(self.bufx, self._Tim)
        x = sum(self._bufx)/32
        
        self._ADCy = ADC(Pin(self.Xp, self._PA))
        Pin(self.Ym, self._PO).low()
        Pin(self.Yp, self._PO).high()
        Pin(self.Xm, self._PI)

        self._ADCy.read_timed(self._bufy, self._Tim)
        y = sum(self._bufy)/32
        
        xfin = self.Kxx * x + self.Kxy * y + self.x0
        yfin = self.Kyy * y + self.Kyx * x + self.y0
        return [xfin, yfin, self.z_scan()]
        
    def calib(self, beta):
        '''!@brief      Sets the calibration coefficients.
            @details    In order to get good readings from the touch panel,
                        we need to adjust our ADC readings. We could do this
                        simply by stretching/compressing our ADC values in each
                        direction and then applying an offset. However, since
                        the touch panels could have slight rotation or shear,
                        we account for this by doing a least-squares linear
                        regression on the board readings to come up with these
                        coefficients.
            @param beta List holding the calibration coefficients for the touch panel.
        '''
        ## @brief Calibration coefficient for the x-direction based on the x-read ADC value.
        #
        self.Kxx = beta[0]
        
        ## @brief Calibration coefficient for the x-direction based on the y-read ADC value.
        #
        self.Kxy = beta[1]
        
        ## @brief Calibration coefficient for the y-direction based on the x-read ADC value.
        #
        self.Kyx = beta[2]
        
        ## @brief Calibration coefficient for the y-direction based on the y-read ADC value.
        #
        self.Kyy = beta[3]
        
        ## @brief Calibration offset in the x-direction.
        #
        self.x0  = beta[4]
        
        ## @brief Calibration offset in the y-direction.
        self.y0  = beta[5]
        pass
        

if __name__ == '__main__':
    # Testing to see how fast we can scan.
    touch = Panel(Pin.cpu.A1, Pin.cpu.A7, Pin.cpu.A0, Pin.cpu.A6)
    idx = 0
    t = ticks_us()
    while idx < 1000:
        touch.xyz_scan()
        idx += 1
        pass
    print(ticks_diff(ticks_us(),t)/1000)
            
    
