'''!@file          taskData.py
    @brief          A file to manage data collection from the system.
    @details        taskData receives input from taskUser based on when the user
                    wants data to be collected. Once signaled, taskData will fill
                    up its arrays and then create a text file creating text files
                    containing all the data it just took. It has three data sets
                    it can record: x-data, y-data, and xy-positions. For x-data
                    and y-data, the data collected is everything related to that
                    positional variable. For example, x-data contains the x-position,
                    x velocity, angle about the y-axis, and angular velocity about
                    the y-axis. The reason for the axis switch is because the
                    x-position and velocity are counteracted by a rotation in
                    the y-direction. This same logic can be applied to the y-data.
                    
                    Note: Only one set of data can be recorded at a time, and
                    after doing so the code becomes momentarily blocking in
                    order to write to a file. 
                    
                    Please reference the finite state machine below for how the
                    task operates:
                        
                    @image html TPDataStates.JPG
                    
                    As always, feel free to reference the source code at
                    https://bitbucket.org/seanwahl2023/me_305_labs/src/master/Term%20Project/
                    however especially in this case since function variables
                    are not captured by Doxygen.
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           March 16, 2022
'''

import array, micropython, gc
from time import ticks_us, ticks_add, ticks_diff

def taskDataFunction(taskName, period, xdataFlag, ydataFlag, posdataFlag, x, xdot, y, ydot, thMeas1, wMeas1, thMeas2, wMeas2, collectionFlag, doneFlag):
    '''!@brief              Routinely checks for data flags and then collects requested data.
        @details            This generator function, given the right set of
                            inputs, is able to constantly check for user calls
                            for data collection and when requested will take
                            data at 750 time points. It has four states. State
                            0 initializes the arrays. State 1 waits for user
                            calls for data collection. State 2 collects data
                            until the arrays are full. State 3 writes this data
                            to a text file and then goes back to State 1.
        @param taskName     The arbitrary, chosen name of the task.
        @param period       The time interval at which this task should operate.
        @param xdataFlag    A shared variable indicating x-related data should be taken.
        @param ydataFlag    A shared variable indicating y-related data should be taken.
        @param posdataFlag  A shared variable indicating that x and y position data should be taken.
        @param x            A shared variable holding the ball's x-position from the touchpad.
        @param xdot         A shared variable holding the ball's x-velocity from the touchpad.
        @param y            A shared variable holding the ball's y-position from the touchpad.
        @param ydot         A shared variable holding the ball's y-velocity from the touchpad.
        @param thMeas1      A shared variable holding the IMU read platform euler angle about the x-axis.
        @param wMeas1       A shared variable holding the IMU read platform angular velocity about the x-axis.
        @param thMeas2      A shared variable holding the IMU read platform euler angle about the y-axis.
        @param wMeas2       A shared variable holding the IMU read platform angular velocity about the y-axis.
        @param collectionFlag   A shared variable indicating when data is done being collected.
        @param doneFlag     A shared variable indicating when data is done being written to a file.
    '''
    
    ## @brief Represents the initialization state of the task.
    #
    S0_INIT = micropython.const(0)
    
    ## @brief Represents the idling state of the task.
    #
    S1_WAIT = micropython.const(1)
    
    ## @brief Represents the data taking state of the task.
    #
    S2_READ = micropython.const(2)    
    
    ## @brief Represents the file writing state of the task.
    S3_WRITE = micropython.const(3)
    
    ## @brief       Denotes the state that the task is currently running in.
    #  @details     taskData only has four possible states. Reference the
    #               finite state machine in taskData.py for details.
    #
    state = S0_INIT
    
    ## @brief The initial time at which the task has first started running.
    #
    start_time = ticks_us()
    
    ## @brief       The next time the task should run.
    #  @details     By using the set period input, next_time is a calculated
    #               value of when the task should run again in order to be
    #               operating at said period.
    #
    next_time = ticks_add(start_time, period)
    
    while True:
        ## @brief The current time when the function tries to run again.
        #
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:    
            
            next_time = ticks_add(next_time, period)
            
            # 
            if state == S0_INIT:
                ## @brief Time array for data collection.
                #
                tArray = array.array('f', 750*[0])
                gc.collect()
                ## @brief Position array for data collection.
                #
                xArray    = array.array('f', tArray)
                ## @brief Velocity array for data collection.
                #
                xdotArray = array.array('f', tArray)
                ## @brief Angle array for data collection.
                #
                thArray   = array.array('f', tArray)
                ## @brief Angular velocity array for data collection.
                wArray    = array.array('f', tArray)
                
                state = S1_WAIT
                yield None
            # 
            elif state == S1_WAIT:
                if xdataFlag.read():
                    ## @brief The start time of data collection.
                    #
                    t = ticks_us()
                    ## @brief Data taking index.
                    #
                    idx = 0
                    ## @brief Data set distinction variable.
                    #
                    datset = 'x'
                    ## @brief The prescribed filename to be written to.
                    #
                    filename = 'xdata.txt'
                    state = S2_READ
                    xdataFlag.write(False)
                    yield None
                elif ydataFlag.read():
                    t = ticks_us()
                    idx = 0
                    datset = 'y'
                    filename = 'ydata.txt'
                    state = S2_READ
                    ydataFlag.write(False)
                    yield None
                elif posdataFlag.read():
                    t = ticks_us()
                    idx = 0
                    datset = 'xy'
                    filename = 'xydata.txt'
                    state = S2_READ
                    posdataFlag.write(False)
                    yield None
                
                yield None
                
                    
            elif state == S2_READ:
                if datset == 'x':
                    tArray[idx]    = ticks_diff(ticks_us(),t)/1_000_000
                    xArray[idx]    = x.read()
                    xdotArray[idx] = xdot.read()
                    thArray[idx]   = thMeas2.read()
                    wArray[idx]    = wMeas2.read()
                    idx += 1
                    
                    if idx == 750:
                        state = S3_WRITE
                        idx = 0
                        collectionFlag.write(True)
                        yield None
                    yield None
                        
                elif datset == 'y':
                    tArray[idx]    = ticks_diff(ticks_us(),t)/1_000_000
                    xArray[idx]    = y.read()
                    xdotArray[idx] = ydot.read()
                    thArray[idx]   = thMeas1.read()
                    wArray[idx]    = wMeas1.read()
                    idx += 1
                    
                    if idx == 750:
                        state = S3_WRITE
                        idx = 0
                        collectionFlag.write(True)
                        yield None
                        
                    yield None
                
                elif datset == 'xy':
                    tArray[idx]    = ticks_diff(ticks_us(),t)/1_000_000
                    xArray[idx]    = x.read()
                    xdotArray[idx] = y.read()
                    idx += 1
                    
                    if idx == 750:
                        state = S3_WRITE
                        idx = 0
                        collectionFlag.write(True)
                        yield None
                        
                    yield None
                yield None
                    
            elif state == S3_WRITE:
                with open(filename,'w') as f:
                    for (item1, item2, item3, item4, item5) in zip(tArray, xArray, xdotArray, thArray, wArray):
                        f.write(f"{item1:.2f}, {item2:.2f}, {item3:.2f}, {item4:.2f}, {item5:.2f}\r\n")
                state = S1_WAIT
                doneFlag.write(True)
                yield None
            
            
                    
            # Just in case
            else:
                print('something went wrong')
                break
        else:
            yield None
    
    
    