'''!@file          taskPanel.py
    @brief          A file to set up and retrieve data from the touch panel.
    @details        taskPanel is able to calibrate the touch panel and then
                    continuously retrieve x and y positions and velocities. 
                    After initialization, it starts trying to read data from
                    the touch panel. Upon user input from taskUser, it can begin
                    a calibration procedure (or read from a file) in order to 
                    generate calibration coefficients for better readings. 
                    
                    For a finite state machine, reference the following diagram:
                        
                    @image html TPPanelStates.JPG
                    
                    As always, feel free to reference the source code at
                    https://bitbucket.org/seanwahl2023/me_305_labs/src/master/Term%20Project/
                    however especially in this case since function variables
                    are not captured by Doxygen.
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           March 16, 2022
'''

import micropython, os
from panel import Panel
from pyb import Pin
from time import ticks_us, ticks_add, ticks_diff

def taskPanelFunction(taskName, period, x, y, xdot, ydot, contact, touchFlag, betamat, fileFlag):
    '''!@brief              Touch panel reading task.
        @details            taskPanel constantly tries to read positions from the
                            touch panel. It can be calibrated from a file or can
                            be manually calibrated so that its readings are 
                            remapped to our platform's surface. When it is reading
                            positions, it will only accept values if contact
                            is true. During this time, we use alpha beta filtering
                            to improve the values we get. If contact is lost,
                            the filter will assume it is moving at a constant
                            velocity for the next second to project its position.
                            After that all readings are set back to 0 until contact
                            is reinitiated.
        @param taskName     The arbitrary chosen name of the task.
        @param period       The time interval at which this task should operate.
        @param x            A shared variable holding the ball's x-position from the touchpad.
        @param y            A shared variable holding the ball's y-position from the touchpad.
        @param xdot         A shared variable holding the ball's x-velocity from the touchpad.
        @param ydot         A shared variable holding the ball's y-velocity from the touchpad.
        @param contact      A shared variable representing when the ball is contacting the touch panel.
        @param touchFlag    A shared variable indicating when the user wants the touch panel to be calibrated.
        @param betamat      A shared variable holding the touch panel calibration coefficients.
        @param fileFlag     A shared variable to designate when the IMU or touch panel have calibrated off a file.
    '''
    
    ## @brief Represents the initializtion state of the task.
    #
    S0_INIT = micropython.const(0)
    
    ## @brief Represents the touch panel reading state of the task.
    #
    S1_UPD = micropython.const(1)
    
    ## @brief Represents the calibration state of the task.
    #
    S2_CALIB = micropython.const(2)
    
    ## @brief       Denotes the state that the task is currently running in.
    #  @details     taskPanel has 3 possible states. The initialization state
    #               sets up the panel object and some of the filtering values.
    #               State 1 is the update state where touch panel values are
    #               constantly read from the panel. State 2 is where the touch
    #               panel can be calibrated.
    #
    state = S0_INIT
    
    ## @brief The time at which the task is first run.
    #
    start_time = ticks_us()
    
    ## @brief       The next time the task should run.
    #  @details     By using the set period input, next_time is a calculated
    #               value of when the task should run again in order to be
    #               operating at said period.
    #
    next_time = ticks_add(start_time, period)
    
    while True:
        ## @brief The current time when the function tries to run again.
        #
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:    
            
            next_time = ticks_add(next_time, period)
            
            # Initialization state.
            if state == S0_INIT:
                ## @brief Touch panel object.
                #
                touch = Panel(Pin.cpu.A1, Pin.cpu.A7, Pin.cpu.A0, Pin.cpu.A6)
                
                ## @brief Alpha value for alpha beta filtering.
                #
                alpha = 0.85
                ## @brief Beta value for alpha beta filtering.
                #
                beta = 0.1
                
                ## @brief Filtered x-position.
                #
                x_hat = 0
                ## @brief Filtered y-position.
                #
                y_hat = 0
                ## @brief Estimated x velocity.
                #
                vx_hat = 0
                ## @brief Estimated y velocity.
                #
                vy_hat = 0
                
                ## @brief Last time that the task run at.
                #
                last_time = ticks_us()
                ## @brief A time value used for projecting the position after contact is lost.
                #
                tcheck= ticks_us()
                
                state = S1_UPD
                yield None
            
            # Value reading state. 
            elif state == S1_UPD:
                
                if touchFlag.read():
                    fileFlag.write(False)
                    _path = os.getcwd()
                    _dir_list = os.listdir(_path)
                    if 'RT_cal_coeffs.txt' in _dir_list:
                        fileFlag.write(True)
                        _betabuf = []
                        
                        with open('RT_cal_coeffs.txt', 'r') as f:
                            _buf = f.read()
                            
                        _buf2 = _buf.split(",")

                        for byte in _buf2:
                            _betabuf.append(float(byte))
                            
                        touch.calib(_betabuf)

                        touchFlag.write(False)
                        yield None
                    else:
                        state = S2_CALIB
                    yield None
                else:
                    ## @brief Time it took to get back to reading the position
                    #           from the last run.
                    #
                    Ts = ticks_diff(ticks_us(),last_time)/1_000_000
                    ## @brief The real x, y, and z values read from the touch
                    #           panel.
                    #
                    x_r,y_r,z_r = touch.xyz_scan()
                    
                    contact.write(z_r)
                    if z_r == True:
                        x_hat = x_hat + Ts*vx_hat
                        ## @brief Innovation term for filtering in the x-direction.
                        #
                        dx = x_r - x_hat
                        x_hat = x_hat + alpha*dx
                        vx_hat = vx_hat + beta/Ts*dx
                        
                        y_hat = y_hat + Ts*vy_hat
                        ## @brief Innovation term for filtering in the y-direction.
                        #
                        dy = y_r - y_hat
                        y_hat = y_hat + alpha*dy
                        vy_hat = vy_hat + beta/Ts*dy
                        
                        x.write(x_hat)
                        y.write(y_hat)
                        
                        xdot.write(vx_hat)
                        ydot.write(vy_hat)
                        
                        tcheck = ticks_us()
                        last_time = ticks_us()
                        yield None
                    else:
                        if ticks_diff(ticks_us(),tcheck) >= 1_000_000:
                            x_hat = 0
                            y_hat = 0
                            vx_hat = 0
                            vy_hat = 0
                            
                            x.write(0)
                            y.write(0)
                            xdot.write(0)
                            ydot.write(0)
                            last_time = ticks_us()
                            yield None
                        else:
                            x_hat = x_hat + Ts*vx_hat
                            y_hat = y_hat + Ts*vy_hat
                            x.write(x_hat)
                            y.write(y_hat)
                            xdot.write(vx_hat)
                            ydot.write(vy_hat)
                            last_time = ticks_us()
                            yield None
                        yield None
                    
                    yield None
                yield None
            
            # Calibration state.
            elif state == S2_CALIB:
                x.write(touch.x_scan())
                y.write(touch.y_scan())
                contact.write(touch.z_scan())
                
                if not touchFlag.read():
                    _tempmat = betamat.read()
                    _row1 = _tempmat[0]
                    _row2 = _tempmat[1]
                    _row3 = _tempmat[2]
                    _betabuf = [_row1[0], _row2[0], _row1[1], _row2[1], _row3[0], _row3[1]]
                    touch.calib(_betabuf)
                        
                    with open('RT_cal_coeffs.txt','w') as f:
                        f.write(f"{_betabuf[0]}, {_betabuf[1]}, {_betabuf[2]}, {_betabuf[3]}, {_betabuf[4]}, {_betabuf[5]}\r\n")
                
                    state = S1_UPD
                    yield None
                else:
                    yield None
                    
                yield None
                
            
                    
            # Just in case
            else:
                print('something went wrong')
                break
        else:
            yield None
        
    
    
    