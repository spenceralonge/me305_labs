'''!!@file           taskMotorTP.py
    @brief          A file to interact and communicate with motors.
    @details        By using taskUser to input values, taskMotor is able
                    to set duty cycles on the individual motors it controls.
                    For the term project, taskMotor has 3 states. The first state
                    sets up the motor objects for future use. The second state is
                    kind of like a waiting room until closed loop control is enabled,
                    and the third state is for when closed loop control is
                    activated. In this state, the motors have their duty cycles
                    set to the gains calculated by the closed loop task. 
                    
                    For a finite state machine, reference the following diagram:
                        
                    @image html TPMotorStates.JPG
                    
                    As always, feel free to reference the source code at
                    https://bitbucket.org/seanwahl2023/me_305_labs/src/master/Term%20Project/
                    however especially in this case since function variables
                    are not captured by Doxygen.
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           March 3, 2022
'''

import micropython
from motorTP import Motor
from pyb import Pin, Timer
from time import ticks_us, ticks_add, ticks_diff

def taskMotorFunction(taskName, period, wFlag, L1, L2):
    '''!@brief              Motor control task.
        @details            taskMotor constantly tries to update the duty cycles
                            of motors 1 and 2 based on what the closed loop task 
                            calculates. It runs on a set period and will
                            only run at times that meet this period. It only has
                            three states, one of which intiializes motors. 
                            State 1 is where the task waits for closed loop control
                            to be enabled. State 2 is entered 
                            through user input and is where closed loop control
                            to balance the platform occurs. State 1 and State 2
                            can easily be toggled between.
        @param taskName     The arbitrary chosen name of the task.
        @param period       The time interval at which this task should operate.
        @param wFlag        A shared variable indicating when the closed loop control should be active.
        @param L1           A shared variable holding the required duty cycle 
                            of motor 1 as calculated by taskClosedLoop.
        @param L1           A shared variable holding the required duty cycle 
                            of motor 2 as calculated by taskClosedLoop.
    '''
    
    ## @brief Represents the initialization state of the task.
    #
    S0_INIT = micropython.const(0)
    
    ## @brief Represents the waiting state of the task.
    #
    S1_UPD = micropython.const(1)
    
    ## @brief Represents the closed loop control state of the task.
    #
    S2_CLC = micropython.const(2)

    ## @brief       Denotes the state that the task is currently running in.
    #  @details     taskMotor has only three states. Please reference the state
    #               diagram present in taskMotorTP.py for details.
    #
    state = S0_INIT
    
    ## @brief The initial time at which the task has first started running.
    #
    start_time = ticks_us()
    
    ## @brief       The next time the task should run.
    #  @details     By using the set period input, next_time is a calculated
    #               value of when the task should run again in order to be
    #               operating at said period.
    #
    next_time = ticks_add(start_time, period)
    
    while True:
        ## @brief The current time when the function tries to run again.
        #
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:    
            
            next_time = ticks_add(next_time, period)
            
            # Set up motor driver and motor objects.
            if state == S0_INIT:
                ## @brief The timer on which the motor driver will run.
                #
                tim = Timer(3, freq = 20000)
                
                ## @brief Object for motor 1.
                #
                #Motor 2 moves in positive y-axis
                motor_2 = Motor(tim, Pin.cpu.B4, Pin.cpu.B5, 1, 2)
                
                ## @brief Object for motor 2.
                #
                #Motor 1 moves in positive x-axis
                motor_1 = Motor(tim, Pin.cpu.B0, Pin.cpu.B1, 3, 4)

                state = S1_UPD
                yield None
            
            # Waiting for CLC to be enabled.
            elif state == S1_UPD:
                # Check for an enable request.
                if wFlag.read():
                    state = S2_CLC
                    yield None
            
            # Allows for closed loop control when wFlag is on.
            elif state == S2_CLC:
                if not wFlag.read():
                    state = S1_UPD
                    yield None
                    
                else:
                    motor_1.set_duty(L1.read())
                    motor_2.set_duty(L2.read())
                    yield None
                    
            # Just in case
            else:
                print('something went wrong')
                break
        else:
            yield None