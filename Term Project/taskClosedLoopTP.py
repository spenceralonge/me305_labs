'''!@file           taskClosedLoopTP.py
    @brief          A file to set closed loop torques (duty cycles) for the motors.
    @details        This file sets up a task to perform motor control during
                    closed loop control mode. It constantly calculates what the
                    duty cycle of the motor should be to reach a value that
                    allows for the platform to be balanced. It does this with user 
                    input gains and values measured from an IMU. It has only 
                    two states. State 1 initializes the motor controllers and 
                    then goes to State 1. State 1 constantly updates the user 
                    set gains in case of changes and then calculates the 
                    necessary duty cycles and outputs it to the user and motor task via a 
                    shared variable. 
                    
                    For a state transition diagram, reference the following:
                        
                    @image html TPCLCStates.JPG
                    
                    As always, feel free to reference the source code at
                    https://bitbucket.org/seanwahl2023/me_305_labs/src/master/Term%20Project/
                    however especially in this case since function variables
                    are not captured by Doxygen.
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           March 16, 2022
'''

import micropython, closedloopTP
from time import ticks_us, ticks_add, ticks_diff

def taskClosedLoopFunction(taskName, period, L1, L2, wMeas1, wMeas2, thMeas1, thMeas2, x, y, xdot, ydot, Kpthy, Kdthy, Kpx, Kdx, xgain, ygain):
    '''!@brief              Routinely calculates the necessary torques (duty cycles) for the motors.
        @details            In this closed loop control, the controller takes in the
                            current rotation of the platform and its angular velocities
                            in order to figure out proper duty cycles to apply
                            to the motors to bring the platform back to level.
                            We do this with gains that allow tuning of the platform's
                            response. Proportional gain acts on the error in the
                            angles of the platform, derivative gain acts on the
                            error in the angular velocities of the platform, and
                            integral gain acts on the systematic error in the
                            platform in bringing it to level. The states necessary
                            to perform this are a initialization state to set
                            up the controllers, and then an update state to constantly
                            calculate what the duty cycles of the motors should be.
        @param taskName     The arbitrary, chosen name of the task.
        @param period       The time interval at which this task should operate.
        @param L1           A shared variable for the duty cycle (Torque) of the motor 1.
        @param L2           A shared variable for the duty cycle (Torque) of the motor 2.
        @param wMeas1       A shared variable holding the IMU read platform 
                            angular velocity about the x-axis.
        @param wMeas2       A shared variable holding the IMU read platform 
                            angular velocity about the y-axis.
        @param thMeas1      A shared variable holding the IMU read platform 
                            euler angle about the x-axis.
        @param thMeas2      A shared variable holding the IMU read platform 
                            euler angle about the y-axis.
        @param x            A shared variable holding the ball's x-position from the touchpad.
        @param y            A shared variable holding the ball's y-position from the touchpad.
        @param xdot         A shared variable holding the ball's x-velocity from the touchpad.
        @param ydot         A shared variable holding the ball's y-velocity from the touchpad.
        @param Kpthy        A shared variable holding the closed inner loop proportional gain, acting on the angles of the platform.      
        @param Kdthy        A shared variable holding the closed inner loop derivative gain, acting on the angular velocities of the platform.    
        @param Kpx          A shared variable holding the closed outer loop proportional gain, acting on the position of the ball.    
        @param Kdx          A shared variable holding the closed outer loop derivative gain, acting on the velocity of the ball.    
        @param xgain        A shared variable holding the duty cycle offset needed to make up for motor bias.
        @param ygain        A shared variable holding the duty cycle offset needed to make up for motor bias.
    '''
    
    ## @brief Represents the initialization state of the task.
    #
    S0_INIT = micropython.const(0)
    
    ## @brief Represents the updating state of the task.
    #
    S1_UPD = micropython.const(1)
    
    ## @brief       Denotes the state that the task is currently running in.
    #  @details     taskClosedLoop has two possible states. State 0 initializes
    #               the motor controllers and always sends the task to state 1. 
    #               State 1 then constantly calculates the duty
    #               cycles needed by the motors.
    #
    state = S0_INIT
    
    ## @brief The initial time at which the task has first started running.
    #
    start_time = ticks_us()
    
    ## @brief       The next time the task should run.
    #  @details     By using the set period input, next_time is a calculated
    #               value of when the task should run again in order to be
    #               operating at said period.
    #
    next_time = ticks_add(start_time, period)

    
    while True:
        ## @brief The current time when the function tries to run again.
        #
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:    
            
            next_time = ticks_add(next_time, period)
            
            # Sets up motor controller object.
            if state == S0_INIT:
                ## @brief The motor controller object for motor 1 (x-axis rotation)
                #
                controller = closedloopTP.ClosedLoop()
                
                ## @brief The motor controller object for motor 2 (y-axis rotation)
                #
                controller2 = closedloopTP.ClosedLoop()
                state = S1_UPD
                yield None
            
            # Constantly updates the gains and required duty cycle
            elif state == S1_UPD:
                controller.set_gain(Kpx.read(), Kdx.read(), Kpthy.read(), Kdthy.read())
                controller2.set_gain(Kpx.read(), Kdx.read(), Kpthy.read(), Kdthy.read())
                L1.write(controller.run(thMeas1.read(), wMeas1.read(), -y.read(), -ydot.read())+xgain.read())
                L2.write(controller2.run(thMeas2.read(), wMeas2.read(), x.read(), xdot.read())+ygain.read())

            # Just in case
            else:
                print('something went wrong')
                break
        else:
            yield None
