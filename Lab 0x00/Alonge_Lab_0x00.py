'''!@file                Alonge_Lab_0x00.py
    @brief               Lab 0x00 - Fibonacci
    @details             Code that computes the Fibonacci number at a user-given index.
    
    @author              Spencer Alonge
'''

def fib(index):
    '''!@brief              Computes Fibonacci number at given index.
        @details            Recursively computes the next Fib numbers in the
                            sequence. Returns the last value computed.
                            
        @param taskName     The arbitrary, chosen name of the task.
    '''
    next = 0;               ## 'next' stores the most recent Fib number               
    if index == 0:          ## If user inputs '0', set 'next' to 0
        next = 0
    elif index ==1:         ## If user inputs '1', set 'next' to 1
        next = 1
    else:
        count = 0           ## Otherwise, add previous two computed
        n0 = 0              ## numbers to 'next' and return it
        n1 = 1
        while (count <= index):
            next = n0 + n1
            n0 = n1
            n1 = next
            count = count + 1
    return next

if __name__ == '__main__':     ## User interface
    user_index = input('Please enter a valid integer: ')
    try:
        user_index = int(user_index)
        if user_index >= 0:
            user_fib = fib(user_index)
            print(f"The Fibonacci number at sequence {user_index} is {user_fib}.")
        else:
            print("Please enter a positive number.")
        
    except:
        print("Please enter a valid whole number.")
    






