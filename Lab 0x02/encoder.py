# -*- coding: utf-8 -*-
"""
Created on Thu Jan 20 08:10:52 2022

@file AlongeAnderson0x02.py
@brief [insert description]
@details
@author Scott Anderson
@author Spencer Alonge
@date January 20, 2022
"""
import pyb

class Encoder:
    '''!@brief          Interface with quadrature encoders.
        @details        The encoder sends positional information
                        via USB and completes multiple tasks simultaneously.
                        A timer is used to update the count 100x per second;
                        if the count jumps too high/low in one data collection,
                        the auto-reload value is added/subtracted to the
                        total count to correctly record the current position.
    '''
    
    def __init__(self,timnumber, channel1port, channel2port, pinnum):
        '''!@brief      Constructs an encoder object.
            @details    Receives information about the encoder's count (ticks)
                        via USB. A timer object is constructed to continually
                        update the recorded position of the encoder.'
            @param timnumber        The port number of the STM32L476RG timer.
            @param channel1port     Data input port of encoder model.
            @param channel2port     Data input port of encoder model.
            @param pinnum           Pin number used on STM32L476RG.
        '''
        self.pin_a = pyb.Pin(channel1port, pyb.Pin.AF_PP, pull=pyb.Pin.PULL_NONE, af=pinnum)
        
        self.pin_b = pyb.Pin(channel2port, pyb.Pin.AF_PP, pull=pyb.Pin.PULL_NONE, af=pinnum)
        
        self.timer = pyb.Timer(timnumber, prescaler=0, period=65535)

        self.channel = self.timer.channel(1,pyb.Timer.ENC_AB)
        
        self.total =0       # Current Encoder position
        self.count =0       # Timer count up to +- AR value
        self.prevcount =0   # Previous tick count
        self.delta =0       # Difference between current/previous tick count

        print('Creating encoder object.\nPress ctrl-c to cancel.\nPress z to'\
              ' zero the position of the encoder.\nPress p to print the current'\
              ' position of the encoder.\nPress d to print the current delta'\
              ' of the encoder.\nPress g to collect data for 30s.\nPress'\
              ' s to stop data collection prematurely.')
        
    def update(self):
        '''!@brief      Updates encoder position and delta.
            @details    prevcount is set to current count.
                        count is set to next count. delta is
                        the difference between the two objects.
                        update() checks to see if delta is too
                        large/small. This indicates overflow
                        or underflow. If this condition is met,
                        the AR value is added or subtracted to
                        the current total. 
        '''
       
        self.prevcount = self.count
        self.count = self.timer.counter()
        self.delta = self.count - self.prevcount
        
        if self.delta < -32768:         # Overflow Condition
            self.delta += (32768*2)
        elif self.delta > 32768:        # Underflow Condition
            self.delta -= (32768*2)

        self.total += self.delta

    def get_position(self):
        '''!@brief      Returns encoder position.
            @details    Position of the encoder shaft is keept
                        track of by counting ticks.
            @return     The position of the encoder shaft.
        '''
        return self.total
    
    def zero(self):
        '''!@brief      Resets the encoder position to zero.
            @details    Sets total count to zero and sets
                        the previous count equal to the current count.
                        This ensures delta will be wil respect to 
                        zeroed count, not previous unzeroed count.
        '''
        self.total =0
        self.prevcount = self.count
        print('Setting position back to zero')
        
    def get_delta(self):
        '''!@brief      Returns encoder delta.
            @details    Delta is the difference between the current and
                        previous counts, computed in update() method.
            @return     The change in position of the encoder shaft
                        between the two most recent updates
        '''
        return self.delta
    