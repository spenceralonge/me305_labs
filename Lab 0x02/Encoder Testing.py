# -*- coding: utf-8 -*-
"""
Created on Thu Jan 20 09:32:49 2022

@author: Spencer Alonge
@author: Scott Anderson
"""
import pyb
import encoder

#pin_a = pyb.Pin('PB6', pyb.Pin.AF_PP, pull=pyb.Pin.PULL_NONE, af=pyb.Pin.AF2_TIM4)

#pin_b = pyb.Pin('PB7', pyb.Pin.AF_PP, pull=pyb.Pin.PULL_NONE, af=pyb.Pin.AF2_TIM4)

#timer = pyb.Timer(4, prescaler=0, period=100000)

#channel = timer.channel(1,pyb.Timer.ENC_AB)
please = encoder.Encoder(4,'PB6','PB7',pyb.Pin.AF2_TIM4)

while True:
    try: 
        please.update()
        print(please.get_position())
        pyb.delay(200)
        
    except KeyboardInterrupt:
        break