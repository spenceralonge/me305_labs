# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 08:41:59 2022

@author: Scott Anderson
@author: Spencer Alonge
"""
import shares
import Task_User, task_encoder

zFlag = shares.Share(False)
pshare = shares.Share(0)
dshare = shares.Share(0)

if __name__ == '__main__':
    task1 = Task_User.taskUserFcn('Task User', 10_000, zFlag, pshare, dshare )
    task2 = task_encoder.taskEncFcn('Task Encoder', 10_000, zFlag, pshare, dshare)
    taskList = {task1, task2}
    
    while True:     # Simultaneously executes user and encoder tasks
        try:
            for task in taskList:
                next(task1)
                next(task2)
            
        except KeyboardInterrupt:
            break
            
print('Program Terminating...')