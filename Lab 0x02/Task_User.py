# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 08:49:03 2022

@author: Scott Anderson
@author: Spencer Alonge
"""
from time import ticks_us, ticks_add, ticks_diff
from pyb import USB_VCP
import shares
import array

def taskUserFcn(taskName, period, zflag, pshare, dshare ):
    '''!@brief          Task that receives information from user via USB
                        and determines what state the FSM is in.
        @details        Input parameters are shared with Encoder task
                        so that both tasks can be completed simultaneously
        @param taskName The name of the task.
        @param period   The amount of time between each update (microseconds).
        @param zflag    Represents whether the z key has been pressed.
        @param pshare   Stores current position. Shared among tasks.
        @param dshare   Stores current delta. Shared among tasks.
    '''

    state = 1       # State of FSM
    
    start_time = ticks_us() # Time stamp of when generator should complete
    next_time = ticks_add(start_time, period) # next iteration
    
    serport = USB_VCP() # Recieve information via USB
    
    while True: # continually iterates until interrupt
        current_time = ticks_us()
        
        if ticks_diff(current_time, next_time) >= 0: # adds period to current time to compute next time
            next_time = ticks_add(next_time, period)
            
            if state == 1: # Waits for user input, continually updates encoder info
                if serport.any(): # If user has pressed a key...
                    charIn = serport.read(1).decode() # ...read that key.
                    
                    if charIn in {'z', 'Z'}: # Zero encoder command z
                        print('Zeroing Encoder...')
                        zflag.write(True)
                        state = 2
                        
                    elif charIn in {'p', 'P'}: # Return position command p
                        print(f'Current Position:{pshare.read()}')
                        
                    elif charIn in {'d', 'D'}: # Return delta command d
                        print(f'Current Delta:{dshare.read()}')
                        
                    elif charIn in {'g', 'G'}: # Collect 30s data command g. Cancel with s.
                        timeArray = array.array('l', 3001*[-1])
                        posArray = array.array('l', 3001*[-1])
                        idx = 0
                        print('Collecting 30s of data. Press s to cancel.')
                        state = 3
                        coll_time = current_time
                        
                    else:
                        print('Unrecognized Input')
            
            elif state == 2: # State that resets encoder position to zero.
                if not zflag.read(): # Sets state back to 1.
                    state = 1
                    print('Encoder Zeroed.')
                    
            elif state == 3: # Creates an array to collect 30s (max) worth of data
                if ticks_diff(current_time, coll_time) <= 30000000:
                    timeArray[idx] = ticks_diff(current_time, coll_time)
                    posArray[idx] = pshare.read()
                    idx += 1
                    
                    if serport.any():
                        charIn = serport.read(1).decode()
                        
                        if charIn in {'s', 'S'}:
                            print('canceled')
                            state = 4
                            idx = 0
                    
                else:
                    print('done')
                    state = 4
                    idx = 0
            
            elif state == 4: # If data collection done, state 4 returns data to user.
                if idx == 3001:
                    state = 1
                    print(f'End of Data')
                elif timeArray[idx] >= 0:
                    print(f'Time: {round(timeArray[idx]/1_000_000, 2)} s, Position: {posArray[idx]}')
                    idx += 1
                else:
                    state = 1
                    print(f'End of Data')
        else:
            yield None
