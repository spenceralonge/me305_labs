'''!@file           taskMotor.py
    @brief          A file to interact and communicate with motors.
    @details        By using taskUser to input values, taskMotor is able
                    to set duty cycles on the individual motors it controls.
                    By setting up motors through the motor driver, it can also
                    detect errors and relay that information to task user. This
                    small subset of motor functionality only requires three states:
                    State 0 intitializes the driver and motors, State 1 constantly
                    tries to set motor duty cycles, and State 2 is triggered
                    by fault detection and will have the motors remain inactive
                    until the user clears the fault. Reference the finite state
                    machine as needed:
                        
                    @image html Lab3MotorStates.JPG
                    
                    As always, feel free to reference the source code at
                    https://bitbucket.org/seanwahl2023/me_305_labs/src/master/Lab%203/
                    however especially in this case since function variables
                    are not captured by Doxygen.
    @author         Sean Wahl
    @author         Grant Gabrielson
    @date           February 16, 2022
'''

import micropython
from DRV8847 import DRV8847
from pyb import Pin, Timer
from time import ticks_us, ticks_add, ticks_diff

def taskMotorFunction(taskName, period, DC1, DC2, cFlag, err):
    '''!@brief              Motor control task.
        @details            taskMotor constantly tries to update the duty cycles
                            of motors 1 and 2. It runs on a set period and will
                            only run at times that meet this period. It only has
                            three states, one of which intiializes the motor
                            driver and motors. State 1 is where the task
                            sets the DC values to whatever taskUser has 
                            designated. State 2 is entered through fault detection
                            and will keep the motors inactive until a cFlag is
                            raised to clear the motors. A cFlag can also be raised
                            in State 1 but it will arbitrarily enable the motors.
        @param taskName     The arbitrary chosen name of the task.
        @param period       The time interval at which this task should operate.
        @param DC1          A shared variable holding the duty cycle of motor 1.
        @param DC2          A shared variable holding the duty cycle of motor 2.
        @param cFlag        A shared variable that means the motors should be re-enabled.
        @param err          A shared variable indicating an fault has been detected.
    '''
    
    ## @brief Represents the initialization state of the task.
    #
    S0_INIT = micropython.const(0)
    
    ## @brief Represents the duty cycle setting state of the task.
    #
    S1_UPD = micropython.const(1)
    
    ## @brief Represents the fault detected error state of the task.
    #
    S2_ERR = micropython.const(2)
    
    ## @brief       Denotes the state that the task is currently running in.
    #  @details     taskMotor has three possible states. State 0 initializes
    #               the motor driver and motors. In State 1, the task is constantly 
    #               setting the motors' duty cycles in case there's a change. 
    #               This is so that we don't have to detect user input in here, 
    #               we can just always be reading a shared variable. State 2 is 
    #               an error state and is entered when the user presses c or when 
    #               a fault is detected. This state resets the motors by 
    #               re-enabling them and transition back to the update state 
    #               after they are reset.
    #
    state = S0_INIT
    
    ## @brief The initial time at which the task has first started running.
    #
    start_time = ticks_us()
    
    ## @brief       The next time the task should run.
    #  @details     By using the set period input, next_time is a calculated
    #               value of when the task should run again in order to be
    #               operating at said period.
    #
    next_time = ticks_add(start_time, period)

    
    while True:
        ## @brief The current time when the function tries to run again.
        #
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:    
            
            next_time = ticks_add(next_time, period)
            
            # Set up motor driver and motor objects.
            if state == S0_INIT:
                ## @brief The timer on which the motor driver will run.
                #
                tim = Timer(3, freq = 20000)
                ## @brief The sleep pin used to enable/disable the motors.
                #
                nSleep = Pin(Pin.cpu.A15, mode = Pin.OUT_PP)
                ## @brief The fault pin used for fault detection interrupts.
                #
                nFault = Pin(Pin.cpu.B2, mode = Pin.IN)
                
                ## @brief The motor driver object.
                #
                motor_drv = DRV8847(tim, nSleep, nFault)
                
                ## @brief Object for motor 1.
                #
                motor_1 = motor_drv.motor(Pin.cpu.B4, Pin.cpu.B5, 1, 2)
                ## @brief Object for motor 2.
                motor_2 = motor_drv.motor(Pin.cpu.B0, Pin.cpu.B1, 3, 4)
                
                # Enable the motors
                motor_drv.enable()
                
                state = S1_UPD
                yield None
            
            # Constantly update the motor duty cycles.
            elif state == S1_UPD:
                # Check for an enable request.
                if cFlag.read():
                    motor_drv.enable()
                    cFlag.write(False)
                    yield None
                
                # Check for a motor error.
                elif motor_drv.faultState:
                    state = S2_ERR
                    err.write(True)
                    yield None
                    
                # Set duty cycles.
                else:
                    motor_1.set_duty(DC1.read())
                    motor_2.set_duty(DC2.read())
                    yield None
            
            # Disable the motors until the fault is cleared.
            elif state == S2_ERR:
                DC1.write(0)
                DC2.write(0)
                motor_1.set_duty(0)
                motor_2.set_duty(0)
                if cFlag.read():
                    motor_drv.enable()
                    cFlag.write(False)
                    state = S1_UPD
                    yield None
                else:
                    yield None
            
            # Just in case
            else:
                print('something went wrong')
                break
        else:
            yield None